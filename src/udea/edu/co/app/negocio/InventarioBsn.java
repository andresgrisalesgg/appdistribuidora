package udea.edu.co.app.negocio;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Inventario;

public interface InventarioBsn {
	void crearInventario(Inventario inventario) throws LlaveDuplicadaException;
	void modificarInventario(Inventario inventario) throws LlaveNoExiste;
	void eliminarInventario(IntegerProperty id) throws LlaveNoExiste;
	Inventario consultarInventario(IntegerProperty id);
	ObservableList<Inventario> getInventarios();
	
	String getInorden();

}
