package udea.edu.co.app.negocio;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Persona;

public interface PersonaBsn {
	void crearPersona(Persona persona) throws LlaveDuplicadaException;
	void modificarPersona(Persona persona) throws LlaveNoExiste;
	void eliminarPersona(IntegerProperty id) throws LlaveNoExiste;
	Persona consultarPersona(IntegerProperty id);
	ObservableList<Persona> getPersonas();
	
	String getInorden();
}
