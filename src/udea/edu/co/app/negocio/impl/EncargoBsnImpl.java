package udea.edu.co.app.negocio.impl;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.EncargoDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.dao.impl.EncargoDAONIOFile;
import udea.edu.co.app.modelo.Encargo;
import udea.edu.co.app.negocio.EncargoBsn;

public class EncargoBsnImpl implements EncargoBsn {

	EncargoDAO encargoDAO;
	
	public EncargoBsnImpl() {
		encargoDAO = new EncargoDAONIOFile();
	}
	
	@Override
	public void crearEncargo(Encargo encargo) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		try {
			encargoDAO.crearEncargo(encargo);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			throw new LlaveDuplicadaException();
		}

	}

	@Override
	public void modificarEncargo(Encargo encargo) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			encargoDAO.modificarEncargo(encargo);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarEnacargo(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			encargoDAO.eliminarEnacargo(id);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Encargo consultarEncargo(IntegerProperty id) {
		// TODO Auto-generated method stub
		return encargoDAO.consultarEncargo(id);
	}

	@Override
	public ObservableList<Encargo> getEncargos() {
		// TODO Auto-generated method stub
		return encargoDAO.getEncargos();
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return encargoDAO.getInorden();
	}

}
