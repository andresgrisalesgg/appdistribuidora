package udea.edu.co.app.negocio.impl;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.DetalleEncargoDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.dao.impl.DetalleEncargoDAONIOFile;
import udea.edu.co.app.modelo.DetalleEncargo;
import udea.edu.co.app.negocio.DetalleEncargoBsn;

public class DetalleEncargoBsnImpl implements DetalleEncargoBsn {
	
	public DetalleEncargoDAO detalleEncargoDAO;
	
	public DetalleEncargoBsnImpl() {
		detalleEncargoDAO = new DetalleEncargoDAONIOFile();
	}

	@Override
	public void crearDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		try {
			detalleEncargoDAO.crearDetalleEncargo(detalle_encargo);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			throw new LlaveDuplicadaException();
		}
	}

	@Override
	public void modificarDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			detalleEncargoDAO.modificarDetalleEncargo(detalle_encargo);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarDetalleEncargo(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			detalleEncargoDAO.eliminarDetalleEncargo(id);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public DetalleEncargo consultarDetalleEncargo(IntegerProperty lote) {
		// TODO Auto-generated method stub
		return detalleEncargoDAO.consultarDetalleEncargo(lote);
	}

	@Override
	public ObservableList<DetalleEncargo> getDetalleEncargos() {
		// TODO Auto-generated method stub
		return detalleEncargoDAO.getDetalleEncargos();
	}

	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return detalleEncargoDAO.getInorden();
	}

}
