package udea.edu.co.app.negocio.impl;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.InventarioDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.dao.impl.InventarioDAONIOFile;
import udea.edu.co.app.modelo.Inventario;
import udea.edu.co.app.negocio.InventarioBsn;

public class InventarioBsnImpl implements InventarioBsn {
	
	public InventarioDAO inventarioDAO;
	
	public InventarioBsnImpl() {
		inventarioDAO = new InventarioDAONIOFile();
	}

	@Override
	public void crearInventario(Inventario inventario) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		try {
			inventarioDAO.crearInventario(inventario);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			throw new LlaveDuplicadaException();
		}

	}

	@Override
	public void modificarInventario(Inventario inventario) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			inventarioDAO.modificarInventario(inventario);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarInventario(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			inventarioDAO.eliminarInventario(id);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Inventario consultarInventario(IntegerProperty id) {
		// TODO Auto-generated method stub
		return inventarioDAO.consultarInventario(id);
	}

	@Override
	public ObservableList<Inventario> getInventarios() {
		// TODO Auto-generated method stub
		return inventarioDAO.getInventarios();
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return inventarioDAO.getInorden();
	}

}
