package udea.edu.co.app.negocio.impl;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.PersonaDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.dao.impl.PersonaDAONIOFile;
import udea.edu.co.app.modelo.Persona;
import udea.edu.co.app.negocio.PersonaBsn;

public class PersonaBsnImpl implements PersonaBsn {
	
	private PersonaDAO personaDAO;
	
	public PersonaBsnImpl() {
		personaDAO = new PersonaDAONIOFile();
	}

	@Override
	public void crearPersona(Persona persona) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		try {
			personaDAO.crearPersona(persona);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			
			throw new LlaveDuplicadaException();
		}

	}

	@Override
	public void modificarPersona(Persona persona) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			personaDAO.modificarPersona(persona);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarPersona(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			personaDAO.eliminarPersona(id);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Persona consultarPersona(IntegerProperty id) {
		// TODO Auto-generated method stub
		return personaDAO.consultarPersona(id.get());
	}

	@Override
	public ObservableList<Persona> getPersonas() {
		// TODO Auto-generated method stub
		return personaDAO.getPersonas();
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return personaDAO.getInorden();
	}

}
