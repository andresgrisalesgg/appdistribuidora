package udea.edu.co.app.negocio.impl;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.LicorDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.dao.impl.LicorDAONIOFile;
import udea.edu.co.app.modelo.Licor;
import udea.edu.co.app.negocio.LicorBsn;

public class LicorBsnImpl implements LicorBsn {
	
	public LicorDAO licorDAO;
	
	public LicorBsnImpl() {
		licorDAO = new LicorDAONIOFile();
	}

	@Override
	public void crearLicor(Licor licor) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		try {
			licorDAO.crearLicor(licor);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			
			throw new LlaveDuplicadaException();
		}
	}

	@Override
	public void modificarLicor(Licor licor) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			licorDAO.modificarLicor(licor);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarLicor(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		try {
			licorDAO.eliminarLicor(id);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Licor consultarLicor(IntegerProperty id) {
		// TODO Auto-generated method stub
		return licorDAO.consultarLicor(id);
	}

	@Override
	public ObservableList<Licor> getLicores() {
		// TODO Auto-generated method stub
		return licorDAO.getLicores();
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return licorDAO.getInorden();
	}

}
