package udea.edu.co.app.util;

import java.time.LocalDate;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.modelo.DetalleEncargo;
import udea.edu.co.app.modelo.Encargo;
import udea.edu.co.app.modelo.Inventario;
import udea.edu.co.app.modelo.Licor;
import udea.edu.co.app.modelo.Persona;
import udea.edu.co.app.negocio.DetalleEncargoBsn;
import udea.edu.co.app.negocio.EncargoBsn;
import udea.edu.co.app.negocio.InventarioBsn;
import udea.edu.co.app.negocio.LicorBsn;
import udea.edu.co.app.negocio.PersonaBsn;
import udea.edu.co.app.negocio.impl.DetalleEncargoBsnImpl;
import udea.edu.co.app.negocio.impl.EncargoBsnImpl;
import udea.edu.co.app.negocio.impl.InventarioBsnImpl;
import udea.edu.co.app.negocio.impl.LicorBsnImpl;
import udea.edu.co.app.negocio.impl.PersonaBsnImpl;

//Esta clase permite hacerle pruebas al DAO de cada entidad
public class PruebasDAO {
	/*//para crear una persona. Cambiar siempre el id para evitar excepcion de clave duplicada
			//crearPersona();
			//mostrarPersonas();
			 //crearLicor();
			//mostrarLicores();
			//crearInventario();
			//mostrarInventarios();
			//crearDetalleEncargo();
			//mostrarDetalleEncargos();
			//crearEncargo();
			//mostrarEncargos();
	public static void crearEncargo() {
		EncargoBsn encargoBsnImpl = new EncargoBsnImpl();
		Encargo encargo = new Encargo();
		
		encargo.setId(new SimpleIntegerProperty(3));
		encargo.setFecha_registrado(new SimpleObjectProperty<LocalDate>(LocalDate.now()));
		encargo.setFecha_envio(new SimpleObjectProperty<LocalDate>(LocalDate.of(2000, 10, 11)));
		encargo.setFecha_recepcion(new SimpleObjectProperty<LocalDate>(LocalDate.of(2020, 9, 3)));
		encargo.setId_persona(new SimpleIntegerProperty(12));
		encargo.setDescripcion(new SimpleStringProperty("un encargo x"));
		encargo.setValor_total(new SimpleIntegerProperty(2300));
		encargo.setPlaca_vehiculo_transportador(new SimpleStringProperty("AC2015"));
		encargo.setNombre_transportador(new SimpleStringProperty("pepito"));
		encargo.setNombre_persona_envia(new SimpleStringProperty("lucho"));
		encargo.setNombre_persona_recibe(new SimpleStringProperty("lucia"));
		encargo.setEstado(new SimpleStringProperty("EN PROCESO"));
		
		encargoBsnImpl.crearEncargo(encargo);
	}
	
	public static void mostrarEncargos() {
		EncargoBsn encargoBsnImpl = new EncargoBsnImpl();
		for(Encargo encargo: encargoBsnImpl.getEncargos()) {
			System.out.println(encargo.getId().get()+" "+encargo.getId_persona().get()+" "+encargo.getFecha_envio().get().toString()+
					" "+encargo.getFecha_registrado().get().toString()+" "+encargo.getEstado().get());
		}
	}
	
	public static void crearDetalleEncargo() {
		DetalleEncargoBsn detalleEncargoBsnImpl = new DetalleEncargoBsnImpl();
		DetalleEncargo detalleEncargo = new DetalleEncargo();
		
		detalleEncargo.setLote(new SimpleIntegerProperty(4));
		detalleEncargo.setId_encargo(new SimpleIntegerProperty(34));
		detalleEncargo.setId_licor(new SimpleIntegerProperty(25));
		detalleEncargo.setNumero_unidades(new SimpleIntegerProperty(200));
		detalleEncargo.setFecha_elaboracion(new SimpleObjectProperty<LocalDate>(LocalDate.of(2000, 11, 23)));
		detalleEncargo.setValor_total(new SimpleIntegerProperty(2000));
		
		detalleEncargoBsnImpl.crearDetalleEncargo(detalleEncargo);
	}
	
	public static void mostrarDetalleEncargos() {
		DetalleEncargoBsn detalleEncargoBsnImpl = new DetalleEncargoBsnImpl();
		for(DetalleEncargo detalle_encargo: detalleEncargoBsnImpl.getDetalleEncargos()) {
			System.out.println(detalle_encargo.getLote().get()+" "+detalle_encargo.getFecha_elaboracion().get().toString());
		}
	}
	
	public static void crearInventario() {
		InventarioBsn inventarioBsn = new InventarioBsnImpl();
		Inventario inventario = new Inventario();
		
		inventario.setId(new SimpleIntegerProperty(2));
		inventario.setExistencias(new SimpleIntegerProperty(200));
		inventario.setEntradas(new SimpleIntegerProperty(150));
		inventario.setSalidas(new SimpleIntegerProperty(350));
		inventarioBsn.crearInventario(inventario);
		
		
	}
	
	public static void mostrarInventarios() {
		InventarioBsn inventarioBsn = new InventarioBsnImpl();
		for(Inventario inventario: inventarioBsn.getInventarios()) {
			System.out.println(inventario.getId().get()+" "+inventario.getExistencias()+" etc...");
		}
	}
	
	public static void crearLicor() {
		LicorBsn licorBns = new LicorBsnImpl();
		Licor licor = new Licor();
		
		licor.setId(new SimpleIntegerProperty(16));
		licor.setTipo(new SimpleStringProperty("Guarito"));
		licor.setMarca(new SimpleStringProperty("Antioqueno"));
		licor.setPorcentaje_alcohol(new SimpleIntegerProperty(30));
		licor.setCapacidad_botella(new SimpleIntegerProperty(200));
		licor.setPrecio_entrada(new SimpleIntegerProperty(2000));
		licor.setPrecio_salida(new SimpleIntegerProperty(2500));
		licor.setId_inventario(new SimpleIntegerProperty(8));
		licorBns.crearLicor(licor);
	}
	
	public static void crearPersona() {
		PersonaBsn personaBsn = new PersonaBsnImpl();
		Persona persona = new Persona();
		
		//Esto es estatico, hay que cambiar el id cada vez que se ejecute
		persona.setId(new SimpleIntegerProperty(22));
		persona.setNombres(new SimpleStringProperty("Andres"));
		persona.setApellidos(new SimpleStringProperty("Grisales Gonzalez"));
		persona.setCompania_representada(new SimpleStringProperty("Compania x89"));
		persona.setEmail(new SimpleStringProperty("email@x.c"));
		persona.setDireccion(new SimpleStringProperty("crra 28a"));
		persona.setTelefono(new SimpleStringProperty("334545"));
		persona.setTipo(new SimpleStringProperty("Cliente"));
		try {
			personaBsn.crearPersona(persona);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void mostrarPersonas() {
		PersonaBsn personaBsn = new PersonaBsnImpl();
		for(Persona persona: personaBsn.getPersonas()) {
			System.out.println(persona.getId().get()+" "+persona.getTipo().get()+" "+persona.getNombres().get()+" etc...");
		}
	}
	
	public static void mostrarLicores() {
		LicorBsn licorBsn = new LicorBsnImpl();
		for(Licor licor: licorBsn.getLicores()) {
			System.out.println(licor.getId().get()+" "+licor.getTipo().get()+" "+licor.getMarca().get()+" etc...");
		}
	}
*/
}
