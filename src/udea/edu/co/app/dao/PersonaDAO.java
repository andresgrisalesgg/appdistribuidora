package udea.edu.co.app.dao;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Persona;

public interface PersonaDAO
{
	void crearPersona(Persona persona) throws LlaveDuplicadaException;
	void modificarPersona(Persona persona) throws LlaveNoExiste;
	void eliminarPersona(IntegerProperty id) throws LlaveNoExiste;
	Persona consultarPersona(Integer id);
	ObservableList<Persona> getPersonas();
	
	String getInorden();
}
