package udea.edu.co.app.dao;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Licor;

public interface LicorDAO {
	void crearLicor(Licor licor) throws LlaveDuplicadaException;
	void modificarLicor(Licor licor) throws LlaveNoExiste;
	void eliminarLicor(IntegerProperty id) throws LlaveNoExiste;
	Licor consultarLicor(IntegerProperty id);
	ObservableList<Licor> getLicores();
	
	String getInorden();
}
