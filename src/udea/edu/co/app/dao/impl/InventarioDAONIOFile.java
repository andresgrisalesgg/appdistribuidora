package udea.edu.co.app.dao.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.InventarioDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Inventario;
import udea.edu.co.app.modelo.RedBlackBST;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

public class InventarioDAONIOFile implements InventarioDAO{
	
	public static final String NOMBRE_ARCHIVO = "inventario-nio";
	public static final int LONGITUD_ID =20;
	public static final int LONGITUD_EXISTENCIAS=20;
	public static final int LONGITUD_ENTRADAS=20;
	public static final int LONGITUD_SALIDAS=20;
	public static final int LONGITUD_REGISTRO=LONGITUD_ID+LONGITUD_EXISTENCIAS+LONGITUD_ENTRADAS+LONGITUD_SALIDAS;
	public static final String ENCODING="UTF-8";
	
	public static final Path archivo = Paths.get(NOMBRE_ARCHIVO);
	public static RedBlackBST<Integer, Integer> indice = new RedBlackBST<Integer, Integer>();
	public static int direccion = 0;
	
	public InventarioDAONIOFile() {
		if(!Files.exists(archivo)) {
			try {
				Files.createFile(archivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		crearIndice();
	}

	private void crearIndice() {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Inventario inventario = parseRegistroToInventario(registro);
				if(inventario.getId().get() != -1) {
					indice.put(inventario.getId().get(), direccion++);
				}else {
					direccion++;
				}
				
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void crearInventario(Inventario inventario) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		Integer direccion_inventario = indice.get(inventario.getId().intValue());
		if(direccion_inventario != null) {
			throw new LlaveDuplicadaException();
		}
		String registro = inventarioToString(inventario);
		byte[] datos = registro.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(datos);
		
		try(FileChannel fc = FileChannel.open(archivo, APPEND)){
			fc.write(buffer);
			indice.put(inventario.getId().get(), direccion++);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	private String inventarioToString(Inventario inventario) {
		// TODO Auto-generated method stub
		StringBuilder registro = new StringBuilder();
		registro.append(completarCampo(String.valueOf(inventario.getId().get()), LONGITUD_ID));
		registro.append(completarCampo(String.valueOf(inventario.getExistencias().get()), LONGITUD_EXISTENCIAS));
		registro.append(completarCampo(String.valueOf(inventario.getEntradas().get()), LONGITUD_ENTRADAS));
		registro.append(completarCampo(String.valueOf(inventario.getSalidas().get()), LONGITUD_SALIDAS));
		return registro.toString();
	}
	
    private String completarCampo(String campo, int tamanio){
        if(campo.length()>tamanio){
            return campo.substring(0, tamanio);
        }
        return String.format("%1$-"+tamanio+"s", campo);
    }

	@Override
	public void modificarInventario(Inventario inventario) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Integer posicionInventario = indice.get(inventario.getId().get());
		if(posicionInventario != null) {
			String registro = inventarioToString(inventario);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionInventario*LONGITUD_REGISTRO);
				fc.write(buffer);
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarInventario(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Inventario inventario = new Inventario();
		inventario.setId(new SimpleIntegerProperty(-1));
		Integer posicionInventario = indice.get(id.get());
		
		if(posicionInventario != null) {
			String registro = inventarioToString(inventario);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionInventario*LONGITUD_REGISTRO);
				fc.write(buffer);
				indice.delete(id.get());
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Inventario consultarInventario(IntegerProperty id) {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Inventario inventario = parseRegistroToInventario(registro);
				if(inventario.getId().get() == id.get()) {
					return inventario;
				}
				buffer.flip();
			}
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private Inventario parseRegistroToInventario(CharBuffer registro) {
		// TODO Auto-generated method stub
		Inventario inventario = new Inventario();
		
		String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
		registro.position(LONGITUD_ID);
		registro = registro.slice();
		inventario.setId(new SimpleIntegerProperty(Integer.valueOf(id)));
		
		String existencias = registro.subSequence(0, LONGITUD_EXISTENCIAS).toString().trim();
		registro.position(LONGITUD_EXISTENCIAS);
		registro = registro.slice();
		inventario.setExistencias(new SimpleIntegerProperty(Integer.valueOf(existencias)));
		
		String entradas = registro.subSequence(0, LONGITUD_ENTRADAS).toString().trim();
		registro.position(LONGITUD_ENTRADAS);
		registro = registro.slice();
		inventario.setEntradas(new SimpleIntegerProperty(Integer.valueOf(entradas)));
		
		String salidas = registro.subSequence(0, LONGITUD_SALIDAS).toString().trim();
		registro.position(LONGITUD_SALIDAS);
		registro = registro.slice();
		inventario.setSalidas(new SimpleIntegerProperty(Integer.valueOf(salidas)));
		
		
		return inventario;
	}

	@Override
	public ObservableList<Inventario> getInventarios() {
		// TODO Auto-generated method stub
		
		ObservableList<Inventario> inventarios = FXCollections.observableArrayList();
		
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Inventario inventario = parseRegistroToInventario(registro);
				inventarios.add(inventario);
				buffer.flip();
			}
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return inventarios;
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return indice.getInorden();
	}
	

}
