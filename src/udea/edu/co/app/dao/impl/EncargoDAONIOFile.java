package udea.edu.co.app.dao.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.EncargoDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Encargo;
import udea.edu.co.app.modelo.RedBlackBST;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

public class EncargoDAONIOFile implements EncargoDAO{
	
	public static final String NOMBRE_ARCHIVO = "encargo-nio";
	public static final int LONGITUD_ID =20;
	public static final int LONGITUD_FECHA_REGISTRADO=20;
	public static final int LONGITUD_FECHA_ENVIO=20;
	public static final int LONGITUD_FECHA_RECEPCION=20;
	public static final int LONGITUD_ID_PERSONA=20;
	public static final int LONGITUD_DESCRIPCION=50;
	public static final int LONGITUD_VALOR_TOTAL=20;
	public static final int LONGITUD_PLACA_VEHICULO_TRANSPORTADOR=20;
	public static final int LONGITUD_NOMBRE_TRANSPORTADOR=50;
	public static final int LONGITUD_NOMBRE_PERSONA_ENVIA=50;
	public static final int LONGITUD_NOMBRE_PERSONA_RECIBE=50;
	public static final int LONGITUD_ESTADO=20;
	public static final int LONGITUD_REGISTRO=LONGITUD_ID+LONGITUD_FECHA_REGISTRADO+LONGITUD_FECHA_ENVIO+
			LONGITUD_FECHA_RECEPCION+LONGITUD_ID_PERSONA+LONGITUD_DESCRIPCION+LONGITUD_VALOR_TOTAL+
			LONGITUD_PLACA_VEHICULO_TRANSPORTADOR+LONGITUD_NOMBRE_TRANSPORTADOR+LONGITUD_NOMBRE_PERSONA_ENVIA+
			LONGITUD_NOMBRE_PERSONA_RECIBE+LONGITUD_ESTADO;
	public static final String ENCODING = "UTF-8";
	
	public static Path archivo = Paths.get(NOMBRE_ARCHIVO);
	public static RedBlackBST<Integer, Integer> indice = new RedBlackBST<Integer, Integer>();
	public static int direccion = 0;
	
	public EncargoDAONIOFile() {
		if(!Files.exists(archivo)) {
			try {
				Files.createFile(archivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		crearIndice();
	}

	private void crearIndice() {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Encargo encargo = parseRegistroToEncargo(registro);
				if(encargo.getId().get() != -1) {
					indice.put(encargo.getId().get(), direccion++);
				}else {
					direccion++;
				}
				
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void crearEncargo(Encargo encargo) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		Integer direccion_encargo = indice.get(encargo.getId().intValue());
		if(direccion_encargo != null) {
			throw new LlaveDuplicadaException();
		}
		
		//guardar encargo en el archivo
		String registro = parseEncargoToRegistro(encargo);
		byte[] datos = registro.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(datos);
		
		try(FileChannel fc = FileChannel.open(archivo, APPEND)){
			fc.write(buffer);
			indice.put(encargo.getId().get(), direccion++);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private String parseEncargoToRegistro(Encargo encargo) {
		// TODO Auto-generated method stub
		StringBuilder registro = new StringBuilder();
		
		registro.append(completarCampo(String.valueOf(encargo.getId().get()), LONGITUD_ID));
		registro.append(completarCampo(parseLocalDateToString(encargo.getFecha_registrado().get()), LONGITUD_FECHA_REGISTRADO));
		registro.append(completarCampo(parseLocalDateToString(encargo.getFecha_envio().get()), LONGITUD_FECHA_ENVIO));
		registro.append(completarCampo(parseLocalDateToString(encargo.getFecha_recepcion().get()), LONGITUD_FECHA_RECEPCION));
		registro.append(completarCampo(String.valueOf(encargo.getId_persona().get()), LONGITUD_ID_PERSONA));
		registro.append(completarCampo(encargo.getDescripcion().get(), LONGITUD_DESCRIPCION));
		registro.append(completarCampo(String.valueOf(encargo.getValor_total().get()), LONGITUD_VALOR_TOTAL));
		registro.append(completarCampo(encargo.getPlaca_vehiculo_transportador().get(), LONGITUD_PLACA_VEHICULO_TRANSPORTADOR));
		registro.append(completarCampo(encargo.getNombre_transportador().get(), LONGITUD_NOMBRE_TRANSPORTADOR));
		registro.append(completarCampo(encargo.getNombre_persona_envia().get(), LONGITUD_NOMBRE_PERSONA_ENVIA));
		registro.append(completarCampo(encargo.getNombre_persona_recibe().get(), LONGITUD_NOMBRE_PERSONA_RECIBE));
		registro.append(completarCampo(encargo.getEstado().get(), LONGITUD_ESTADO));
		
		return registro.toString();
	}
	
	private String parseLocalDateToString(LocalDate localDate) {
		// TODO Auto-generated method stub
		StringBuilder fecha = new StringBuilder();
		fecha.append(localDate.getYear()+"-"+localDate.getMonthValue()+"-"+localDate.getDayOfMonth());
		
		return fecha.toString();
	}

	private String completarCampo(String campo, int tamanio) {
		// TODO Auto-generated method stub
		 if(campo.length()>tamanio){
	            return campo.substring(0, tamanio);
	        }
	        return String.format("%1$-"+tamanio+"s", campo);
	}

	@Override
	public void modificarEncargo(Encargo encargo) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Integer posicionEncargo = indice.get(encargo.getId().get());
		if(posicionEncargo != null) {
			String registro = parseEncargoToRegistro(encargo);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionEncargo*LONGITUD_REGISTRO);
				fc.write(buffer);
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarEnacargo(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Encargo encargo = new Encargo();
		encargo.setId(new SimpleIntegerProperty(-1));
		Integer posicionEncargo = indice.get(id.get());
		
		if(posicionEncargo != null) {
			String registro = parseEncargoToRegistro(encargo);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionEncargo*LONGITUD_REGISTRO);
				fc.write(buffer);
				indice.delete(id.get());
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Encargo consultarEncargo(IntegerProperty id) {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Encargo encargo_consultado = parseRegistroToEncargo(registro);
				if(encargo_consultado.getId().get() == id.get()) {
					return encargo_consultado;
				}
				
				buffer.flip();
			}
			
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private Encargo parseRegistroToEncargo(CharBuffer registro) {
		// TODO Auto-generated method stub
		Encargo encargo = new Encargo();
		
		String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
		registro.position(LONGITUD_ID);
		registro = registro.slice();
		encargo.setId(new SimpleIntegerProperty(Integer.parseInt(id)));
		
		LocalDate fecha_registrado = parseStringToLocalDate(registro.subSequence(0, LONGITUD_FECHA_REGISTRADO).toString().trim());
		registro.position(LONGITUD_FECHA_REGISTRADO);
		registro = registro.slice();
		encargo.setFecha_registrado(new SimpleObjectProperty<LocalDate>(fecha_registrado));
		
		LocalDate fecha_envio = parseStringToLocalDate(registro.subSequence(0, LONGITUD_FECHA_ENVIO).toString().trim());
		registro.position(LONGITUD_FECHA_ENVIO);
		registro = registro.slice();
		encargo.setFecha_envio(new SimpleObjectProperty<LocalDate>(fecha_envio));
		
		LocalDate fecha_recepcion = parseStringToLocalDate(registro.subSequence(0, LONGITUD_FECHA_RECEPCION).toString().trim());
		registro.position(LONGITUD_FECHA_RECEPCION);
		registro = registro.slice();
		encargo.setFecha_recepcion(new SimpleObjectProperty<LocalDate>(fecha_recepcion));
		
		String id_persona = registro.subSequence(0, LONGITUD_ID_PERSONA).toString().trim();
		registro.position(LONGITUD_ID_PERSONA);
		registro = registro.slice();
		encargo.setId_persona(new SimpleIntegerProperty(Integer.parseInt(id_persona)));
		
		String descripcion = registro.subSequence(0, LONGITUD_DESCRIPCION).toString().trim();
		registro.position(LONGITUD_DESCRIPCION);
		registro = registro.slice();
		encargo.setDescripcion(new SimpleStringProperty(descripcion));
		
		String valor_total = registro.subSequence(0, LONGITUD_VALOR_TOTAL).toString().trim();
		registro.position(LONGITUD_VALOR_TOTAL);
		registro = registro.slice();
		encargo.setValor_total(new SimpleIntegerProperty(Integer.parseInt(valor_total)));
		
		String placa_vehiculo_transportador = registro.subSequence(0, LONGITUD_PLACA_VEHICULO_TRANSPORTADOR).toString().trim();
		registro.position(LONGITUD_PLACA_VEHICULO_TRANSPORTADOR);
		registro = registro.slice();
		encargo.setPlaca_vehiculo_transportador(new SimpleStringProperty(placa_vehiculo_transportador));
		
		String nombre_transportador = registro.subSequence(0, LONGITUD_NOMBRE_TRANSPORTADOR).toString().trim();
		registro.position(LONGITUD_NOMBRE_TRANSPORTADOR);
		registro = registro.slice();
		encargo.setNombre_transportador(new SimpleStringProperty(nombre_transportador));
		
		String nombre_persona_envia = registro.subSequence(0, LONGITUD_NOMBRE_PERSONA_ENVIA).toString().trim();
		registro.position(LONGITUD_NOMBRE_PERSONA_ENVIA);
		registro = registro.slice();
		encargo.setNombre_persona_envia(new SimpleStringProperty(nombre_persona_envia));
		
		String nombre_persona_recibe = registro.subSequence(0, LONGITUD_NOMBRE_PERSONA_RECIBE).toString().trim();
		registro.position(LONGITUD_NOMBRE_PERSONA_RECIBE);
		registro = registro.slice();
		encargo.setNombre_persona_recibe(new SimpleStringProperty(nombre_persona_recibe));
		
		String estado = registro.subSequence(0, LONGITUD_ESTADO).toString().trim();
		registro.position(LONGITUD_ESTADO);
		registro = registro.slice();
		encargo.setEstado(new SimpleStringProperty(estado));
		
		
		return encargo;
	}
	
	private LocalDate parseStringToLocalDate(String trim) {
		// TODO Auto-generated method stub
		String[] arrayFecha = trim.split("-");
		LocalDate fecha = LocalDate.of(Integer.valueOf(arrayFecha[0]), Integer.valueOf(arrayFecha[1])
				, Integer.valueOf(arrayFecha[2]));
		
		return fecha;
	}

	@Override
	public ObservableList<Encargo> getEncargos() {
		// TODO Auto-generated method stub
		ObservableList<Encargo> encargos = FXCollections.observableArrayList();
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Encargo encargo_consultado = parseRegistroToEncargo(registro);
				encargos.add(encargo_consultado);
				
				buffer.flip();
			}
			
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return encargos;
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return indice.getInorden();
	}

}
