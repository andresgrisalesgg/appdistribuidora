package udea.edu.co.app.dao.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.DetalleEncargoDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.DetalleEncargo;
import udea.edu.co.app.modelo.RedBlackBST;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

public class DetalleEncargoDAONIOFile implements DetalleEncargoDAO{
	
	public static final String NOMBRE_ARCHIVO = "detalleencargo-nio";
	public static final int LONGITUD_LOTE =20;
	public static final int LONGITUD_ID_ENCARGO=20;
	public static final int LONGITUD_ID_LICOR=20;
	public static final int LONGITUD_NUMERO_UNIDADES=20;
	public static final int LONGITUD_FECHA_ELABORACION=20;
	public static final int LONGITUD_VALOR_TOTAL=20;
	public static final int LONGITUD_REGISTRO=LONGITUD_LOTE+LONGITUD_ID_ENCARGO+LONGITUD_ID_LICOR+
			LONGITUD_NUMERO_UNIDADES+LONGITUD_FECHA_ELABORACION+LONGITUD_VALOR_TOTAL;
	public static final String ENCODING = "UTF-8";
	
	public static final Path archivo = Paths.get(NOMBRE_ARCHIVO);
	public static RedBlackBST<Integer, Integer> indice = new RedBlackBST<Integer, Integer>();
	public static int direccion=0;
	
	public DetalleEncargoDAONIOFile() {
		if(!Files.exists(archivo)) {
			try {
				Files.createFile(archivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		crearIndice();
	}

	private void crearIndice() {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				DetalleEncargo detalleEncargo = parseRegistroToDetalleEncargo(registro);
				if(detalleEncargo.getLote().get() != -1) {
					indice.put(detalleEncargo.getLote().get(), direccion++);
				}else {
					direccion++;
				}
				
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	@Override
	public void crearDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		Integer direccion_detalle_encargo = indice.get(detalle_encargo.getLote().get());
		if(direccion_detalle_encargo!= null) {
			throw new LlaveDuplicadaException();
		}
		String registro = parseDetalleEncargoToRegistro(detalle_encargo);
		byte[] datos = registro.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(datos);
		
		try(FileChannel fc = FileChannel.open(archivo, APPEND)){
			fc.write(buffer);
			indice.put(detalle_encargo.getLote().get(), direccion++);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	private String parseDetalleEncargoToRegistro(DetalleEncargo detalle_encargo) {
		// TODO Auto-generated method stub
		StringBuilder registro = new StringBuilder();
		registro.append(completarCampo(String.valueOf(detalle_encargo.getLote().get()), LONGITUD_LOTE));
		registro.append(completarCampo(String.valueOf(detalle_encargo.getId_encargo().get()), LONGITUD_ID_ENCARGO));
		registro.append(completarCampo(String.valueOf(detalle_encargo.getId_licor().get()), LONGITUD_ID_LICOR));
		registro.append(completarCampo(String.valueOf(detalle_encargo.getNumero_unidades().get()), LONGITUD_NUMERO_UNIDADES));
		registro.append(completarCampo(parseLocalDateToString(detalle_encargo.getFecha_elaboracion().get()), LONGITUD_FECHA_ELABORACION));
		registro.append(completarCampo(String.valueOf(detalle_encargo.getValor_total().get()), LONGITUD_VALOR_TOTAL));
		return registro.toString();
	}
	
	private String parseLocalDateToString(LocalDate localDate) {
		// TODO Auto-generated method stub
		StringBuilder fecha = new StringBuilder();
		fecha.append(localDate.getYear()+"-");
		fecha.append(localDate.getMonthValue()+"-");
		fecha.append(localDate.getDayOfMonth());
		return fecha.toString();
	}

	private String completarCampo(String campo, int tamanio) {
		// TODO Auto-generated method stub
		 if(campo.length()>tamanio){
	            return campo.substring(0, tamanio);
	        }
	        return String.format("%1$-"+tamanio+"s", campo);
	}

	@Override
	public void modificarDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Integer posicionDetalleE = indice.get(detalle_encargo.getLote().get());
		if(posicionDetalleE != null) {
			String registro = parseDetalleEncargoToRegistro(detalle_encargo);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionDetalleE*LONGITUD_REGISTRO);
				fc.write(buffer);
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public void eliminarDetalleEncargo(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		DetalleEncargo detalle_encargo = new DetalleEncargo();
		detalle_encargo.setLote(new SimpleIntegerProperty(-1));
		Integer posicionDetalleE = indice.get(id.get());
		
		if(posicionDetalleE != null) {
			String registro = parseDetalleEncargoToRegistro(detalle_encargo);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionDetalleE*LONGITUD_REGISTRO);
				fc.write(buffer);
				indice.delete(id.get());
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public DetalleEncargo consultarDetalleEncargo(IntegerProperty id) {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				DetalleEncargo detalleEncargo = parseRegistroToDetalleEncargo(registro);
				
				if(detalleEncargo.getLote().get() == id.get()) {
					return detalleEncargo;
				}
				
				buffer.flip();
			}
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private DetalleEncargo parseRegistroToDetalleEncargo(CharBuffer registro) {
		// TODO Auto-generated method stub
		DetalleEncargo detalleEncargo = new DetalleEncargo();
		
		String lote = registro.subSequence(0, LONGITUD_LOTE).toString().trim();
		registro.position(LONGITUD_LOTE);
		registro = registro.slice();
		detalleEncargo.setLote(new SimpleIntegerProperty(Integer.parseInt(lote)));
		
		String id_encargo = registro.subSequence(0, LONGITUD_ID_ENCARGO).toString().trim();
		registro.position(LONGITUD_ID_ENCARGO);
		registro = registro.slice();
		detalleEncargo.setId_encargo(new SimpleIntegerProperty(Integer.parseInt(id_encargo)));
		
		String id_licor = registro.subSequence(0, LONGITUD_ID_LICOR).toString().trim();
		registro.position(LONGITUD_ID_LICOR);
		registro = registro.slice();
		detalleEncargo.setId_licor(new SimpleIntegerProperty(Integer.parseInt(id_licor)));
		
		String numero_unidades = registro.subSequence(0, LONGITUD_NUMERO_UNIDADES).toString().trim();
		registro.position(LONGITUD_NUMERO_UNIDADES);
		registro = registro.slice();
		detalleEncargo.setNumero_unidades(new SimpleIntegerProperty(Integer.parseInt(numero_unidades)));
		
		LocalDate fecha_elaboracion = parseStringToLocalDate(registro.subSequence(0, LONGITUD_FECHA_ELABORACION).toString().trim());
		registro.position(LONGITUD_FECHA_ELABORACION);
		registro.slice();
		detalleEncargo.setFecha_elaboracion(new SimpleObjectProperty<LocalDate>(fecha_elaboracion));
		
		String valorTotal = registro.subSequence(0, LONGITUD_VALOR_TOTAL).toString().trim();
		registro.position(LONGITUD_FECHA_ELABORACION);
		detalleEncargo.setValor_total(new SimpleIntegerProperty(Integer.parseInt(valorTotal)));
		
		
		return detalleEncargo;
	}

	private LocalDate parseStringToLocalDate(String trim) {
		// TODO Auto-generated method stub
		String[] arrayFecha = trim.split("-");
		LocalDate fecha = LocalDate.of(Integer.valueOf(arrayFecha[0]), Integer.valueOf(arrayFecha[1])
				, Integer.valueOf(arrayFecha[2]));
		
		return fecha;
	}

	@Override
	public ObservableList<DetalleEncargo> getDetalleEncargos() {
		// TODO Auto-generated method stub
		ObservableList<DetalleEncargo> detalleEncargos = FXCollections.observableArrayList();
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				DetalleEncargo detalleEncargo = parseRegistroToDetalleEncargo(registro);
				
				detalleEncargos.add(detalleEncargo);
				
				buffer.flip();
			}
			
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return detalleEncargos;
	}

	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return indice.getInorden();
	}

}
