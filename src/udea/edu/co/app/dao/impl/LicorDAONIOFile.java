package udea.edu.co.app.dao.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.LicorDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Licor;
import udea.edu.co.app.modelo.RedBlackBST;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

public class LicorDAONIOFile implements LicorDAO{
	
	public static final String NOMBRE_ARCHIVO = "licor-nio";
	public static final int LONGITUD_ID =20;
	public static final int LONGITUD_TIPO=20;
	public static final int LONGITUD_MARCA=20;
	public static final int LONGITUD_PORCENTAJE_ALCOHOL=20;
	public static final int LONGITUD_CAPACIDAD_BOTELLA=20;
	public static final int LONGITUD_PRECIO_ENTRADA=20;
	public static final int LONGITUD_PRECIO_SALIDA=20;
	public static final int LONGITUD_ID_INVENTARIO=20;
	public static final int LONGITUD_REGISTRO=LONGITUD_ID+LONGITUD_TIPO+LONGITUD_MARCA+LONGITUD_PORCENTAJE_ALCOHOL+
			LONGITUD_CAPACIDAD_BOTELLA+LONGITUD_PRECIO_ENTRADA+LONGITUD_PRECIO_SALIDA+LONGITUD_ID_INVENTARIO;
	public static final String ENCODING="UTF-8";
	
	public static final Path archivo = Paths.get(NOMBRE_ARCHIVO);
	private static final RedBlackBST<Integer, Integer> indice = new RedBlackBST<Integer, Integer>();
	private static int direccion=0;
	
	public LicorDAONIOFile() {
		if(!Files.exists(archivo)) {
			try {
				Files.createFile(archivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		crearIndice();
	}
	
	private void crearIndice() {
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Licor licor = parseRegistroToLicor(registro);
				if(licor.getId().get() != -1) {
					indice.put(licor.getId().get(), direccion++);
				}else {
					direccion++;
				}
				
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		
	}

	@Override
	public void crearLicor(Licor licor) throws LlaveDuplicadaException {
		// TODO Auto-generated method stub
		Integer direccion_licor = indice.get(licor.getId().intValue());
		if(direccion_licor != null) {
			throw new LlaveDuplicadaException();
		}
		String registro = parseLicorToString(licor);
		byte[] datos = registro.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(datos);
		try(FileChannel fc = (FileChannel.open(archivo, APPEND))){
			fc.write(buffer);
			indice.put(licor.getId().get(), direccion++);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	private String parseLicorToString(Licor licor) {
		// TODO Auto-generated method stub
		StringBuilder registro = new StringBuilder();
		registro.append(completarCampo(String.valueOf(licor.getId().get()), LONGITUD_ID));
		registro.append(completarCampo(licor.getTipo().get(), LONGITUD_TIPO));
		registro.append(completarCampo(licor.getMarca().get(), LONGITUD_MARCA));
		registro.append(completarCampo(String.valueOf(licor.getPorcentaje_alcohol().get()), LONGITUD_PORCENTAJE_ALCOHOL));
		registro.append(completarCampo(String.valueOf(licor.getCapacidad_botella().get()), LONGITUD_CAPACIDAD_BOTELLA));
		registro.append(completarCampo(String.valueOf(licor.getPrecio_entrada().get()), LONGITUD_PRECIO_ENTRADA));
		registro.append(completarCampo(String.valueOf(licor.getPrecio_salida().get()), LONGITUD_PRECIO_SALIDA));
		registro.append(completarCampo(String.valueOf(licor.getId_inventario().get()), LONGITUD_ID_INVENTARIO));
		return registro.toString();
	}

	private String completarCampo(String campo, int tamanio) {
		// TODO Auto-generated method stub
		 if(campo.length()>tamanio){
	            return campo.substring(0, tamanio);
	        }
	        return String.format("%1$-"+tamanio+"s", campo);
	}

	@Override
	public void modificarLicor(Licor licor) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Integer posicionLicor = indice.get(licor.getId().get());
		if(posicionLicor != null) {
			String registro = parseLicorToString(licor);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionLicor*LONGITUD_REGISTRO);
				fc.write(buffer);
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
		
		
	}

	@Override
	public void eliminarLicor(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Licor licor = new Licor();
		licor.setId(new SimpleIntegerProperty(-1));
		Integer posicionLicor = indice.get(id.get());
		
		if(posicionLicor != null) {
			String registro = parseLicorToString(licor);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionLicor*LONGITUD_REGISTRO);
				fc.write(buffer);
				indice.delete(id.get());
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Licor consultarLicor(IntegerProperty id) {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Licor licor = parseRegistroToLicor(registro);
				
				if(licor.getId().get() == id.get()) {
					return licor;
				}
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private Licor parseRegistroToLicor(CharBuffer registro) {
		// TODO Auto-generated method stub
		Licor licor = new Licor();
		
		String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
		registro.position(LONGITUD_ID);
		registro = registro.slice();
		licor.setId(new SimpleIntegerProperty(Integer.valueOf(id)));
		
		String tipo = registro.subSequence(0, LONGITUD_TIPO).toString().trim();
		registro.position(LONGITUD_TIPO);
		registro = registro.slice();
		licor.setTipo(new SimpleStringProperty(tipo));
		
		String marca = registro.subSequence(0, LONGITUD_MARCA).toString().trim();
		registro.position(LONGITUD_MARCA);
		registro = registro.slice();
		licor.setMarca(new SimpleStringProperty(marca));
		
		String porcentaje_alcohol = registro.subSequence(0, LONGITUD_PORCENTAJE_ALCOHOL).toString().trim();
		registro.position(LONGITUD_PORCENTAJE_ALCOHOL);
		registro = registro.slice();
		licor.setPorcentaje_alcohol(new SimpleIntegerProperty(Integer.parseInt(porcentaje_alcohol)));
		
		String capacidad_botella = registro.subSequence(0, LONGITUD_CAPACIDAD_BOTELLA).toString().trim();
		registro.position(LONGITUD_CAPACIDAD_BOTELLA);
		registro = registro.slice();
		licor.setCapacidad_botella(new SimpleIntegerProperty(Integer.parseInt(capacidad_botella)));
		
		String precio_entrada = registro.subSequence(0, LONGITUD_PRECIO_ENTRADA).toString().trim();
		registro.position(LONGITUD_PRECIO_ENTRADA);
		registro = registro.slice();
		licor.setPrecio_entrada(new SimpleIntegerProperty(Integer.parseInt(precio_entrada)));
		
		String precio_salida = registro.subSequence(0, LONGITUD_PRECIO_SALIDA).toString().trim();
		registro.position(LONGITUD_PRECIO_SALIDA);
		registro = registro.slice();
		licor.setPrecio_salida(new SimpleIntegerProperty(Integer.parseInt(precio_salida)));
		
		String id_inventario = registro.subSequence(0, LONGITUD_ID_INVENTARIO).toString().trim();
		registro.position(LONGITUD_ID_INVENTARIO);
		licor.setId_inventario(new SimpleIntegerProperty(Integer.parseInt(id_inventario)));
		
		return licor;
	}

	@Override
	public ObservableList<Licor> getLicores() {
		// TODO Auto-generated method stub
		ObservableList<Licor> licores = FXCollections.observableArrayList();
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Licor licor = parseRegistroToLicor(registro);
				licores.add(licor);
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return licores;
	}
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return indice.getInorden();
	}

}
