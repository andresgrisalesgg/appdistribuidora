package udea.edu.co.app.dao.impl;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.PersonaDAO;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Persona;
import udea.edu.co.app.modelo.RedBlackBST;

public class PersonaDAONIOFile implements PersonaDAO {
	
	public static final String NOMBRE_ARCHIVO="persona-nio";
	public static final int LONGITUD_ID =20;
	public static final int LONGITUD_TIPO=20;
	public static final int LONGITUD_NOMBRES=50;
	public static final int LONGITUD_APELLIDOS=50;
	public static final int LONGITUD_EMAIL=50;
	public static final int LONGITUD_TELEFONO=20;
	public static final int LONGITUD_COMPANIA_REPRESENTADA=50;
	public static final int LONGITUD_DIRECCION=50;
	public static final int LONGITUD_REGISTRO = LONGITUD_ID+LONGITUD_TIPO+LONGITUD_NOMBRES+LONGITUD_APELLIDOS+
			LONGITUD_EMAIL+LONGITUD_TELEFONO+LONGITUD_COMPANIA_REPRESENTADA+LONGITUD_DIRECCION;
	public static final String ENCODING="UTF-8";
	
	public static final Path archivo = Paths.get(NOMBRE_ARCHIVO);
	private static final RedBlackBST<Integer, Integer> indice = new RedBlackBST<Integer, Integer>();
	public static int direccion=0;
	
	public PersonaDAONIOFile() {
		if(!Files.exists(archivo)) {
			try {
				Files.createFile(archivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		crearIndice();
	}
	

	private void crearIndice() {
		// TODO Auto-generated method stub
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Persona persona = parseRegistroToPersona(registro);
				if(persona.getId().get() != -1) {
					indice.put(persona.getId().get(), direccion++);
				}else {
					direccion++;
				}
				
				buffer.flip();
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
	}


	@Override
	public void crearPersona(Persona persona) throws LlaveDuplicadaException{
		// TODO Auto-generated method stub
		
		Integer direccion_persona = indice.get(persona.getId().intValue());
		
		if(direccion_persona != null) {
			throw new LlaveDuplicadaException();
		}
		//Persona personaConsultada = consultarPersona(direccion_persona);
		String registro = parsePersonaToString(persona);
		byte[] datos = registro.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(datos);
        try(FileChannel fc = (FileChannel.open(archivo, APPEND))){
            fc.write(buffer);
            indice.put(persona.getId().get(), direccion++);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

	}

	private String parsePersonaToString(Persona persona) {
		// TODO Auto-generated method stub
		StringBuilder registro= new StringBuilder();
		registro.append(completarCampo(String.valueOf(persona.getId().get()), LONGITUD_ID));
		registro.append(completarCampo(persona.getTipo().get(), LONGITUD_TIPO));
		registro.append(completarCampo(persona.getNombres().get(), LONGITUD_NOMBRES));
		registro.append(completarCampo(persona.getApellidos().get(), LONGITUD_APELLIDOS));
		registro.append(completarCampo(persona.getEmail().get(), LONGITUD_EMAIL));
		registro.append(completarCampo(persona.getTelefono().get(), LONGITUD_TELEFONO));
		registro.append(completarCampo(persona.getCompania_representada().get(), LONGITUD_COMPANIA_REPRESENTADA));
		registro.append(completarCampo(persona.getDireccion().get(), LONGITUD_DIRECCION));
		
		
		return registro.toString();
	}
	
    private String completarCampo(String campo, int tamanio){
        if(campo.length()>tamanio){
            return campo.substring(0, tamanio);
        }
        return String.format("%1$-"+tamanio+"s", campo);
    }


	@Override
	public void modificarPersona(Persona persona) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Integer posicionPersona = indice.get(persona.getId().get());
		if(posicionPersona != null) {
			String registro = parsePersonaToString(persona);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionPersona*LONGITUD_REGISTRO);
				fc.write(buffer);
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}

	}

	@Override
	public void eliminarPersona(IntegerProperty id) throws LlaveNoExiste {
		// TODO Auto-generated method stub
		Persona persona = new Persona();
		persona.setId(new SimpleIntegerProperty(-1));
		Integer posicionPersona = indice.get(id.get());
		
		if(posicionPersona != null) {
			String registro = parsePersonaToString(persona);
			byte[] datos = registro.getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(datos);
			try(FileChannel fc = (FileChannel.open(archivo, WRITE))){
				fc.position(posicionPersona*LONGITUD_REGISTRO);
				fc.write(buffer);
				indice.delete(id.get());
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}else {
			throw new LlaveNoExiste();
		}
	}

	@Override
	public Persona consultarPersona(Integer posicion) {
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			sbc.position(posicion*LONGITUD_REGISTRO);
			sbc.read(buffer);
			buffer.rewind();
			CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
			Persona personaConvertida = parseRegistroToPersona(registro);
			buffer.flip();
			return personaConvertida;
			

		}catch(IOException ioe) {
			ioe.printStackTrace();
			
		}
		return null;
	}

	private Persona parseRegistroToPersona(CharBuffer registro) {
		// TODO Auto-generated method stub
		Persona persona = new Persona();
		
		String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
		registro.position(LONGITUD_ID);
		registro = registro.slice();
		persona.setId(new SimpleIntegerProperty(Integer.parseInt(id)));
		
		String tipo = registro.subSequence(0, LONGITUD_TIPO).toString().trim();
		registro.position(LONGITUD_TIPO);
		registro = registro.slice();
		persona.setTipo(new SimpleStringProperty(tipo));
		
		String nombres = registro.subSequence(0, LONGITUD_NOMBRES).toString().trim();
		registro.position(LONGITUD_NOMBRES);
		registro = registro.slice();
		persona.setNombres(new SimpleStringProperty(nombres));
		
		String apellidos = registro.subSequence(0, LONGITUD_APELLIDOS).toString().trim();
		registro.position(LONGITUD_APELLIDOS);
		registro = registro.slice();
		persona.setApellidos(new SimpleStringProperty(apellidos));
		
		String email = registro.subSequence(0, LONGITUD_EMAIL).toString().trim();
		registro.position(LONGITUD_EMAIL);
		registro = registro.slice();
		persona.setEmail(new SimpleStringProperty(email));
		
		String telefono = registro.subSequence(0, LONGITUD_TELEFONO).toString().trim();
		registro.position(LONGITUD_TELEFONO);
		registro = registro.slice();
		persona.setTelefono(new SimpleStringProperty(telefono));
		
		String compania_representada = registro.subSequence(0, LONGITUD_COMPANIA_REPRESENTADA).toString().trim();
		registro.position(LONGITUD_COMPANIA_REPRESENTADA);
		registro = registro.slice();
		persona.setCompania_representada(new SimpleStringProperty(compania_representada));
		
		String direccion = registro.subSequence(0, LONGITUD_DIRECCION).toString().trim();
		registro.position(LONGITUD_DIRECCION);
		persona.setDireccion(new SimpleStringProperty(direccion));
		
		
		
		return persona;
	}


	@Override
	public ObservableList<Persona> getPersonas() {
		// TODO Auto-generated method stub
		ObservableList<Persona> personas = FXCollections.observableArrayList();
		
		try(SeekableByteChannel sbc = Files.newByteChannel(archivo)){
			ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
			while(sbc.read(buffer) > 0) {
				buffer.rewind();
				CharBuffer registro = Charset.forName(ENCODING).decode(buffer);
				Persona personaConvertida = parseRegistroToPersona(registro);
				personas.add(personaConvertida);
				buffer.flip();
			}

		}catch(IOException ioe) {
			ioe.printStackTrace();
			
		}
		return personas;
	}
	
	@Override
	public String getInorden() {
		// TODO Auto-generated method stub
		return indice.getInorden();
	}

}
