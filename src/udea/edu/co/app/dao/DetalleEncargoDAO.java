package udea.edu.co.app.dao;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.DetalleEncargo;

public interface DetalleEncargoDAO
{
	void crearDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveDuplicadaException;
	void modificarDetalleEncargo(DetalleEncargo detalle_encargo) throws LlaveNoExiste;
	void eliminarDetalleEncargo(IntegerProperty id) throws LlaveNoExiste;
	DetalleEncargo consultarDetalleEncargo(IntegerProperty id);
	ObservableList<DetalleEncargo> getDetalleEncargos();
	
	String getInorden();

}
