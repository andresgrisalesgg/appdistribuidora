package udea.edu.co.app.dao;

import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Encargo;


public interface EncargoDAO
{
	void crearEncargo(Encargo encargo) throws LlaveDuplicadaException;
	void modificarEncargo(Encargo encargo) throws LlaveNoExiste;
	void eliminarEnacargo(IntegerProperty id) throws LlaveNoExiste;
	Encargo consultarEncargo(IntegerProperty id);
	ObservableList<Encargo> getEncargos();
	
	String getInorden();
}
