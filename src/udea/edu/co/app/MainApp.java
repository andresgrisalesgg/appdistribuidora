package udea.edu.co.app;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import udea.edu.co.app.controlador.DetalleEncargoControlador;
import udea.edu.co.app.controlador.EncargoControlador;
import udea.edu.co.app.controlador.InventarioControlador;
import udea.edu.co.app.controlador.LicorControlador;
import udea.edu.co.app.controlador.PersonaControlador;
import udea.edu.co.app.controlador.RaizControlador;

public class MainApp extends Application{
	
	private Stage primeraVentana;
	private BorderPane raizVista;
	
	public void start(Stage primaryStage) {
		primeraVentana = primaryStage;
		primeraVentana.setTitle("App distribuidora");
		//primeraVentana.setResizable(false);
		cargarVistaRaiz();
		cargarVistaPrincipal();
	}
	
	public void cargarVistaPersona() {
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/personaVista.fxml"));
			AnchorPane vistaPersona = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaPersona);
			PersonaControlador personaControlador = cargador.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void cargarVistaPrincipal() {
		// TODO Auto-generated method stub
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/principalVista.fxml"));
			
			AnchorPane vistaPrincipal = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaPrincipal);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
	}
	
	public void cargarVistaLicor() {
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/licorVista.fxml"));
			AnchorPane vistaLicor = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaLicor);
			LicorControlador licorControlador = cargador.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cargarVistaInventario() {
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/inventarioVista.fxml"));
			AnchorPane vistaInventario = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaInventario);
			InventarioControlador inventarioControlador = cargador.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cargarVistaEncargo() {
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/encargoVista.fxml"));
			AnchorPane vistaEncargo = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaEncargo);
			EncargoControlador encargoControlador = cargador.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cargarDetalleEncargo() {
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/detalleEncargoVista.fxml"));
			AnchorPane vistaDetalleEncargo = (AnchorPane) cargador.load();
			
			raizVista.setCenter(vistaDetalleEncargo);
			DetalleEncargoControlador detalleEncargoControlador = cargador.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void cargarVistaRaiz() {
		// TODO Auto-generated method stub
		
		
		try {
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(getClass().getResource("vista/raizVista.fxml"));
			
			raizVista = (BorderPane) cargador.load();
			
			Scene escena = new Scene(raizVista);
			primeraVentana.setScene(escena);
			
			RaizControlador raizControlador = cargador.getController();
			raizControlador.setMain(this);
			
			primeraVentana.show();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
}
