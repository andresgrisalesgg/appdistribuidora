package udea.edu.co.app.modelo;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Encargo
{
	//PK en entidad ENCARGO
	private IntegerProperty id;
	private ObjectProperty<LocalDate> fecha_registrado;
	private ObjectProperty<LocalDate> fecha_envio;
	private ObjectProperty<LocalDate> fecha_recepcion;
	//FK en entidad ENCARGO hace referencia a la PK en la entidad PERSONA
	private IntegerProperty id_persona;
	private StringProperty descripcion;
	private IntegerProperty valor_total;
	private StringProperty placa_vehiculo_transportador;
	private StringProperty nombre_transportador;
	private StringProperty nombre_persona_envia;
	private StringProperty nombre_persona_recibe;
	private StringProperty estado;
	
	
	
	public Encargo(IntegerProperty id, ObjectProperty<LocalDate> fecha_registrado,
			ObjectProperty<LocalDate> fecha_envio, ObjectProperty<LocalDate> fecha_recepcion,
			IntegerProperty id_persona, StringProperty descripcion, IntegerProperty valor_total,
			StringProperty placa_vehiculo_transportador, StringProperty nombre_transportador,
			StringProperty nombre_persona_envia, StringProperty nombre_persona_recibe, StringProperty estado) {
		super();
		this.id = id;
		this.fecha_registrado = fecha_registrado;
		this.fecha_envio = fecha_envio;
		this.fecha_recepcion = fecha_recepcion;
		this.id_persona = id_persona;
		this.descripcion = descripcion;
		this.valor_total = valor_total;
		this.placa_vehiculo_transportador = placa_vehiculo_transportador;
		this.nombre_transportador = nombre_transportador;
		this.nombre_persona_envia = nombre_persona_envia;
		this.nombre_persona_recibe = nombre_persona_recibe;
		this.estado = estado;
	}
	public Encargo() {
		// TODO Auto-generated constructor stub
		fecha_registrado = new SimpleObjectProperty<LocalDate>(LocalDate.of(1980, 1, 1));
		fecha_envio = new SimpleObjectProperty<LocalDate>(LocalDate.of(1980, 1, 1));
		fecha_recepcion = new SimpleObjectProperty<LocalDate>(LocalDate.of(1980, 1, 1));
		id_persona = new SimpleIntegerProperty(0);
		descripcion = new SimpleStringProperty("");
		valor_total = new SimpleIntegerProperty(0);
		placa_vehiculo_transportador = new SimpleStringProperty("");
		nombre_transportador = new SimpleStringProperty("");
		nombre_persona_envia = new SimpleStringProperty("");
		nombre_persona_recibe = new SimpleStringProperty("");
		estado = new SimpleStringProperty("");
	}
	public IntegerProperty getId() {
		return id;
	}
	public void setId(IntegerProperty id) {
		this.id = id;
	}
	public ObjectProperty<LocalDate> getFecha_registrado() {
		return fecha_registrado;
	}
	public void setFecha_registrado(ObjectProperty<LocalDate> fecha_registrado) {
		this.fecha_registrado = fecha_registrado;
	}
	public ObjectProperty<LocalDate> getFecha_envio() {
		return fecha_envio;
	}
	public void setFecha_envio(ObjectProperty<LocalDate> fecha_envio) {
		this.fecha_envio = fecha_envio;
	}
	public ObjectProperty<LocalDate> getFecha_recepcion() {
		return fecha_recepcion;
	}
	public void setFecha_recepcion(ObjectProperty<LocalDate> fecha_recepcion) {
		this.fecha_recepcion = fecha_recepcion;
	}
	public IntegerProperty getId_persona() {
		return id_persona;
	}
	public void setId_persona(IntegerProperty id_persona) {
		this.id_persona = id_persona;
	}
	public StringProperty getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}
	public IntegerProperty getValor_total() {
		return valor_total;
	}
	public void setValor_total(IntegerProperty valor_total) {
		this.valor_total = valor_total;
	}
	public StringProperty getPlaca_vehiculo_transportador() {
		return placa_vehiculo_transportador;
	}
	public void setPlaca_vehiculo_transportador(StringProperty placa_vehiculo_transportador) {
		this.placa_vehiculo_transportador = placa_vehiculo_transportador;
	}
	public StringProperty getNombre_transportador() {
		return nombre_transportador;
	}
	public void setNombre_transportador(StringProperty nombre_transportador) {
		this.nombre_transportador = nombre_transportador;
	}
	public StringProperty getNombre_persona_envia() {
		return nombre_persona_envia;
	}
	public void setNombre_persona_envia(StringProperty nombre_persona_envia) {
		this.nombre_persona_envia = nombre_persona_envia;
	}
	public StringProperty getNombre_persona_recibe() {
		return nombre_persona_recibe;
	}
	public void setNombre_persona_recibe(StringProperty nombre_persona_recibe) {
		this.nombre_persona_recibe = nombre_persona_recibe;
	}
	public StringProperty getEstado() {
		return estado;
	}
	public void setEstado(StringProperty estado) {
		this.estado = estado;
	}
	
	
	

}
