package udea.edu.co.app.modelo;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class DetalleEncargo
{
	//PK en la entidad DETALLE_ENCARGO
	private IntegerProperty lote;
	//FK en la entidad DETALLE_ENCARGO hace referencia a la PK en la entidad ENCARGO
	private IntegerProperty id_encargo;
	//FK en la entidad DETALLE_ENCARGO hace referencia a la PK en la entidad LICOR
	private IntegerProperty id_licor;
	private IntegerProperty numero_unidades;
	private ObjectProperty<LocalDate> fecha_elaboracion;
	private IntegerProperty valor_total;
	
	
	
	public DetalleEncargo(IntegerProperty lote, IntegerProperty id_encargo, IntegerProperty id_licor,
			IntegerProperty numero_unidades, ObjectProperty<LocalDate> fecha_elaboracion, IntegerProperty valor_total) {
		super();
		this.lote = lote;
		this.id_encargo = id_encargo;
		this.id_licor = id_licor;
		this.numero_unidades = numero_unidades;
		this.fecha_elaboracion = fecha_elaboracion;
		this.valor_total = valor_total;
	}
	public DetalleEncargo() {
		// TODO Auto-generated constructor stub
		this.id_encargo = new SimpleIntegerProperty(0);
		this.id_licor = new SimpleIntegerProperty(0);
		this.numero_unidades = new SimpleIntegerProperty(0);
		this.fecha_elaboracion = new SimpleObjectProperty<LocalDate>(LocalDate.of(1980, 1, 1));
		this.valor_total = new SimpleIntegerProperty(0);
	}
	public IntegerProperty getLote() {
		return lote;
	}
	public void setLote(IntegerProperty lote) {
		this.lote = lote;
	}
	public IntegerProperty getId_encargo() {
		return id_encargo;
	}
	public void setId_encargo(IntegerProperty id_encargo) {
		this.id_encargo = id_encargo;
	}
	public IntegerProperty getId_licor() {
		return id_licor;
	}
	public void setId_licor(IntegerProperty id_licor) {
		this.id_licor = id_licor;
	}
	public IntegerProperty getNumero_unidades() {
		return numero_unidades;
	}
	public void setNumero_unidades(IntegerProperty numero_unidades) {
		this.numero_unidades = numero_unidades;
	}
	public ObjectProperty<LocalDate> getFecha_elaboracion() {
		return fecha_elaboracion;
	}
	public void setFecha_elaboracion(ObjectProperty<LocalDate> fecha_elaboracion) {
		this.fecha_elaboracion = fecha_elaboracion;
	}
	public IntegerProperty getValor_total() {
		return valor_total;
	}
	public void setValor_total(IntegerProperty valor_total) {
		this.valor_total = valor_total;
	}
	
	

}
