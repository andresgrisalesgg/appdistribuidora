package udea.edu.co.app.modelo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Persona
{
	//PK en la entidad PERSONA
	private IntegerProperty id;
	private StringProperty tipo;
	private StringProperty nombres;
	private StringProperty apellidos;
	private StringProperty email;
	private StringProperty telefono;
	private StringProperty compania_representada;
	private StringProperty direccion;
	
	
	public Persona() {
		tipo = new SimpleStringProperty("");
		nombres = new SimpleStringProperty("");
		apellidos = new SimpleStringProperty("");
		email = new SimpleStringProperty("");
		telefono = new SimpleStringProperty("");
		compania_representada = new SimpleStringProperty("");
		direccion = new SimpleStringProperty("");
	}
	
	public Persona(IntegerProperty id, StringProperty tipo, StringProperty nombres, StringProperty apellidos,
			StringProperty email, StringProperty telefono, StringProperty compania_representada,
			StringProperty direccion) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.email = email;
		this.telefono = telefono;
		this.compania_representada = compania_representada;
		this.direccion = direccion;
	}
	public IntegerProperty getId() {
		return id;
	}
	public void setId(IntegerProperty id) {
		this.id = id;
	}
	public StringProperty getTipo() {
		return tipo;
	}
	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}
	public StringProperty getNombres() {
		return nombres;
	}
	public void setNombres(StringProperty nombres) {
		this.nombres = nombres;
	}
	public StringProperty getApellidos() {
		return apellidos;
	}
	public void setApellidos(StringProperty apellidos) {
		this.apellidos = apellidos;
	}
	public StringProperty getEmail() {
		return email;
	}
	public void setEmail(StringProperty email) {
		this.email = email;
	}
	public StringProperty getTelefono() {
		return telefono;
	}
	public void setTelefono(StringProperty telefono) {
		this.telefono = telefono;
	}
	public StringProperty getCompania_representada() {
		return compania_representada;
	}
	public void setCompania_representada(StringProperty compania_representada) {
		this.compania_representada = compania_representada;
	}
	public StringProperty getDireccion() {
		return direccion;
	}
	public void setDireccion(StringProperty direccion) {
		this.direccion = direccion;
	}
	
	

}
