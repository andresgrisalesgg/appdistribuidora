package udea.edu.co.app.modelo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Licor
{
	//PK en la entidad LICOR
	private IntegerProperty id;
	private StringProperty tipo;
	private StringProperty marca;
	private IntegerProperty porcentaje_alcohol;
	private IntegerProperty capacidad_botella;
	private IntegerProperty precio_entrada;
	private IntegerProperty precio_salida;
	//FK en la entidad LICOR hace referencia a la PK en la entidad INVENTARIO
	private IntegerProperty id_inventario;
	
	public Licor() {
		tipo = new SimpleStringProperty("");
		marca = new SimpleStringProperty("");
		porcentaje_alcohol = new SimpleIntegerProperty(0);
		capacidad_botella = new SimpleIntegerProperty(0);
		precio_entrada = new SimpleIntegerProperty(0);
		precio_salida = new SimpleIntegerProperty(0);
		id_inventario = new SimpleIntegerProperty(0);
	}
	
	public Licor(IntegerProperty id, StringProperty tipo, StringProperty marca, IntegerProperty porcentaje_alcohol,
			IntegerProperty capacidad_botella, IntegerProperty precio_entrada, IntegerProperty precio_salida,
			IntegerProperty id_inventario) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.marca = marca;
		this.porcentaje_alcohol = porcentaje_alcohol;
		this.capacidad_botella = capacidad_botella;
		this.precio_entrada = precio_entrada;
		this.precio_salida = precio_salida;
		this.id_inventario = id_inventario;
	}
	
	public IntegerProperty getId() {
		return id;
	}
	public void setId(IntegerProperty id) {
		this.id = id;
	}
	public StringProperty getTipo() {
		return tipo;
	}
	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}
	public StringProperty getMarca() {
		return marca;
	}
	public void setMarca(StringProperty marca) {
		this.marca = marca;
	}
	public IntegerProperty getPorcentaje_alcohol() {
		return porcentaje_alcohol;
	}
	public void setPorcentaje_alcohol(IntegerProperty porcentaje_alcohol) {
		this.porcentaje_alcohol = porcentaje_alcohol;
	}
	public IntegerProperty getCapacidad_botella() {
		return capacidad_botella;
	}
	public void setCapacidad_botella(IntegerProperty capacidad_botella) {
		this.capacidad_botella = capacidad_botella;
	}
	public IntegerProperty getPrecio_entrada() {
		return precio_entrada;
	}
	public void setPrecio_entrada(IntegerProperty precio_entrada) {
		this.precio_entrada = precio_entrada;
	}
	public IntegerProperty getPrecio_salida() {
		return precio_salida;
	}
	public void setPrecio_salida(IntegerProperty precio_salida) {
		this.precio_salida = precio_salida;
	}
	public IntegerProperty getId_inventario() {
		return id_inventario;
	}
	public void setId_inventario(IntegerProperty id_inventario) {
		this.id_inventario = id_inventario;
	}
	
	

}
