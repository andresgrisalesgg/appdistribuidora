package udea.edu.co.app.modelo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Inventario
{
	//PK en la entidad INVENTARIO
	private IntegerProperty id;
	private IntegerProperty existencias;
	private IntegerProperty entradas;
	private IntegerProperty salidas;
	
	public Inventario() {
		existencias = new SimpleIntegerProperty(0);
		entradas = new SimpleIntegerProperty(0);
		salidas = new SimpleIntegerProperty(0);
	}
	
	public Inventario(IntegerProperty id, IntegerProperty existencias, IntegerProperty entradas,
			IntegerProperty salidas) {
		super();
		this.id = id;
		this.existencias = existencias;
		this.entradas = entradas;
		this.salidas = salidas;
	}

	public IntegerProperty getId() {
		return id;
	}
	public void setId(IntegerProperty id) {
		this.id = id;
	}
	public IntegerProperty getExistencias() {
		return existencias;
	}
	public void setExistencias(IntegerProperty existencias) {
		this.existencias = existencias;
	}
	public IntegerProperty getEntradas() {
		return entradas;
	}
	public void setEntradas(IntegerProperty entradas) {
		this.entradas = entradas;
	}
	public IntegerProperty getSalidas() {
		return salidas;
	}
	public void setSalidas(IntegerProperty salidas) {
		this.salidas = salidas;
	}
	
	
	

}
