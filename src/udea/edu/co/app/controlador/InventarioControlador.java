package udea.edu.co.app.controlador;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Inventario;
import udea.edu.co.app.negocio.InventarioBsn;
import udea.edu.co.app.negocio.impl.InventarioBsnImpl;

public class InventarioControlador implements Initializable {
	
	InventarioBsn inventarioBsn;
	@FXML
	private TableView<Inventario> tInventario;
	@FXML
	private TableColumn<Inventario, Integer> cId;
	@FXML
	private TableColumn<Inventario, Integer> cExistencias;
	@FXML
	private TableColumn<Inventario, Integer> cEntradas;
	@FXML
	private TableColumn<Inventario, Integer> cSalidas;
	
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnModificar;
	@FXML
	private Button btnEliminar;
	@FXML
	private Button btnActualizar;
	
	@FXML
	private TextField txtBuscar;
	
	@FXML
	private TextField txtId;
	@FXML
	private TextField txtExistencias;
	@FXML
	private TextField txtEntradas;
	@FXML
	private TextField txtSalidas;
	
	@FXML
	private Button btnVerArbol;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		inventarioBsn = new InventarioBsnImpl();
		
		btnNuevo.setOnAction(e -> {
			actionNuevo();
		});
		
		btnModificar.setOnAction(e -> {
			actionModificar();
		});
		
		btnEliminar.setOnAction(e -> {
			actionEliminar();
		});
		
		btnActualizar.setOnAction(e -> {
			actionActualizar();
		});
		
		actionActualizar();
		
		cId.setCellValueFactory(cellData -> cellData.getValue().getId().asObject());
		cExistencias.setCellValueFactory(cellData -> cellData.getValue().getExistencias().asObject());
		cEntradas.setCellValueFactory(cellData -> cellData.getValue().getEntradas().asObject());
		cSalidas.setCellValueFactory(cellData -> cellData.getValue().getSalidas().asObject());
		
		
		btnVerArbol.setOnAction(e -> {
			JOptionPane.showMessageDialog(null, "Recorrido INORDEN del arbol, por claves: "+inventarioBsn.getInorden());
		});
	}

	private void actionActualizar() {
		// TODO Auto-generated method st
		tInventario.setItems(inventarioBsn.getInventarios());
	}

	public void limpiarCampos(){
		txtId.setText("");
		txtExistencias.setText("");
		txtEntradas.setText("");
		txtSalidas.setText("");
		
	}
	
	public void habilitarCampos() {
		txtId.setEditable(true);
		txtExistencias.setEditable(true);
		txtEntradas.setEditable(true);
		txtSalidas.setEditable(true);
	}
	
	public void deshabilitarCampos() {
		txtId.setEditable(false);
		txtExistencias.setEditable(false);
		txtEntradas.setEditable(false);
		txtSalidas.setEditable(false);
	}

	private void actionEliminar() {
		// TODO Auto-generated method stub
		try {
			inventarioBsn.eliminarInventario(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		limpiarCampos();
	}

	private void actionModificar() {
		// TODO Auto-generated method stub
		Inventario inventario = new Inventario();
		
		inventario.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		inventario.setExistencias(new SimpleIntegerProperty(Integer.parseInt(txtExistencias.getText())));
		inventario.setEntradas(new SimpleIntegerProperty(Integer.parseInt(txtEntradas.getText())));
		inventario.setSalidas(new SimpleIntegerProperty(Integer.parseInt(txtSalidas.getText())));
		
		try {
			inventarioBsn.modificarInventario(inventario);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,"Llave no existe");
		}
		
		limpiarCampos();
		
		
	}

	private void actionNuevo() {
		// TODO Auto-generated method stub
		Inventario inventario = new Inventario();
		
		inventario.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		inventario.setExistencias(new SimpleIntegerProperty(Integer.parseInt(txtExistencias.getText())));
		inventario.setEntradas(new SimpleIntegerProperty(Integer.parseInt(txtEntradas.getText())));
		inventario.setSalidas(new SimpleIntegerProperty(Integer.parseInt(txtSalidas.getText())));
		
		try {
			inventarioBsn.crearInventario(inventario);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave duplicada. No se guardo el registro.");
		}
		
		limpiarCampos();
	}	
	
}
