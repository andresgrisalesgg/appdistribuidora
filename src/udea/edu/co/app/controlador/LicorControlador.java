package udea.edu.co.app.controlador;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Licor;
import udea.edu.co.app.negocio.LicorBsn;
import udea.edu.co.app.negocio.impl.LicorBsnImpl;

public class LicorControlador implements Initializable{
	
	LicorBsn licorBsn;
	
	@FXML
	private TableView<Licor> tLicor;
	@FXML
	private TableColumn<Licor, Integer> cId;
	@FXML
	private TableColumn<Licor, String> cTipo;
	@FXML
	private TableColumn<Licor, String> cMarca;
	@FXML
	private TableColumn<Licor, Integer> cPorcentaje;
	@FXML
	private TableColumn<Licor, Integer> cCapacidad;
	@FXML
	private TableColumn<Licor, Integer> cPrecioE;
	@FXML
	private TableColumn<Licor, Integer> cPrecioS;
	@FXML
	private TableColumn<Licor, Integer> cIdInventario;
	
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnModificar;
	@FXML
	private Button btnEliminar;
	@FXML
	private Button btnActualizar;
	
	@FXML
	private TextField txtBuscar;
	
	@FXML
	private TextField txtId;
	@FXML
	private TextField txtTipo;
	@FXML
	private TextField txtMarca;
	@FXML
	private TextField txtPorcentaje;
	@FXML
	private TextField txtCapacidad;
	@FXML
	private TextField txtPrecioE;
	@FXML
	private TextField txtPrecioS;
	//FK en la entidad LICOR hace referencia a la PK en la entidad INVENTARIO
	@FXML
	private TextField txtIdInventario;
	
	@FXML
	private Button btnVerArbol;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		licorBsn = new LicorBsnImpl();
		
		btnNuevo.setOnAction(e -> {
			actionNuevo();
		});
		
		btnModificar.setOnAction(e -> {
			actionModificar();
		});
		
		btnEliminar.setOnAction(e -> {
			actionEliminar();
		});
		
		btnActualizar.setOnAction(e -> {
			actionActualizar();
		});
		
		actionActualizar();
		
		//configurar columnas
		cId.setCellValueFactory(cellData -> cellData.getValue().getId().asObject());
		cTipo.setCellValueFactory(cellData -> cellData.getValue().getTipo());
		cMarca.setCellValueFactory(cellData -> cellData.getValue().getMarca());
		cPorcentaje.setCellValueFactory(cellData -> cellData.getValue().getPorcentaje_alcohol().asObject());
		cCapacidad.setCellValueFactory(cellData -> cellData.getValue().getCapacidad_botella().asObject());
		cPrecioE.setCellValueFactory(cellData -> cellData.getValue().getPrecio_entrada().asObject());
		cPrecioS.setCellValueFactory(cellData -> cellData.getValue().getPrecio_salida().asObject());
		cIdInventario.setCellValueFactory(cellData -> cellData.getValue().getId_inventario().asObject());
		
		btnVerArbol.setOnAction(e -> {
			JOptionPane.showMessageDialog(null, "Recorrido INORDEN del arbol, por claves: "+licorBsn.getInorden());
		});
	}

	private void actionActualizar() {
		// TODO Auto-generated method stub
		tLicor.setItems(licorBsn.getLicores());
	}

	public void limpiarCampos(){
		txtId.setText("");
		txtTipo.setText("");
		txtMarca.setText("");
		txtPorcentaje.setText("");
		txtCapacidad.setText("");
		txtPrecioE.setText("");
		txtPrecioS.setText("");
		txtIdInventario.setText("");
	}
	
	public void habilitarCampos() {
		txtId.setEditable(true);
		txtTipo.setEditable(true);
		txtMarca.setEditable(true);
		txtPorcentaje.setEditable(true);
		txtCapacidad.setEditable(true);
		txtPrecioE.setEditable(true);
		txtPrecioS.setEditable(true);
		txtIdInventario.setEditable(true);
	}
	
	public void deshabilitarCampos() {
		txtId.setEditable(false);
		txtTipo.setEditable(false);
		txtMarca.setEditable(false);
		txtPorcentaje.setEditable(false);
		txtCapacidad.setEditable(false);
		txtPrecioE.setEditable(false);
		txtPrecioS.setEditable(false);
		txtIdInventario.setEditable(false);
	}

	private void actionEliminar() {
		// TODO Auto-generated method stub
		try {
			licorBsn.eliminarLicor(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		limpiarCampos();
	}

	private void actionModificar() {
		// TODO Auto-generated method stub
		habilitarCampos();
		Licor licor = new Licor();
		
		
		licor.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		licor.setTipo(new SimpleStringProperty(txtTipo.getText()));
		licor.setMarca(new SimpleStringProperty(txtMarca.getText()));
		licor.setPorcentaje_alcohol(new SimpleIntegerProperty(Integer.parseInt(txtPorcentaje.getText())));
		licor.setCapacidad_botella(new SimpleIntegerProperty(Integer.parseInt(txtCapacidad.getText())));
		licor.setPrecio_entrada(new SimpleIntegerProperty(Integer.parseInt(txtPrecioE.getText())));
		licor.setPrecio_salida(new SimpleIntegerProperty(Integer.parseInt(txtPrecioS.getText())));
		licor.setId_inventario(new SimpleIntegerProperty(Integer.parseInt(txtIdInventario.getText())));
		try {
			licorBsn.modificarLicor(licor);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		
		limpiarCampos();
		
	}

	private void actionNuevo() {
		// TODO Auto-generated method stub
		habilitarCampos();
		Licor licor = new Licor();
		
		
		licor.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		licor.setTipo(new SimpleStringProperty(txtTipo.getText()));
		licor.setMarca(new SimpleStringProperty(txtMarca.getText()));
		licor.setPorcentaje_alcohol(new SimpleIntegerProperty(Integer.parseInt(txtPorcentaje.getText())));
		licor.setCapacidad_botella(new SimpleIntegerProperty(Integer.parseInt(txtCapacidad.getText())));
		licor.setPrecio_entrada(new SimpleIntegerProperty(Integer.parseInt(txtPrecioE.getText())));
		licor.setPrecio_salida(new SimpleIntegerProperty(Integer.parseInt(txtPrecioS.getText())));
		licor.setId_inventario(new SimpleIntegerProperty(Integer.parseInt(txtIdInventario.getText())));
		try {
			licorBsn.crearLicor(licor);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave duplicada. No se guardo el registro.");
		}
		limpiarCampos();
	}
}
