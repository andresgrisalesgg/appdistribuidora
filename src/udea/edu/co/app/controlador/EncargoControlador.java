package udea.edu.co.app.controlador;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Encargo;
import udea.edu.co.app.negocio.EncargoBsn;
import udea.edu.co.app.negocio.impl.EncargoBsnImpl;

public class EncargoControlador implements Initializable {
	
	private EncargoBsn encargoBsn;
	
	@FXML
	private TableView<Encargo> tEncargos;
	@FXML
	private TableColumn<Encargo, Integer> cId;
	@FXML
	private TableColumn<Encargo, LocalDate> cFRegistrado;
	@FXML
	private TableColumn<Encargo, LocalDate> cFEnvio;
	@FXML
	private TableColumn<Encargo, LocalDate> cFRecepcion;
	@FXML
	private TableColumn<Encargo, Integer> cIdPersona;
	@FXML
	private TableColumn<Encargo, String> cDescripcion;
	@FXML
	private TableColumn<Encargo, Integer> cValorT;
	@FXML
	private TableColumn<Encargo, String> cPlacaV;
	@FXML
	private TableColumn<Encargo, String> cNombreT;
	@FXML
	private TableColumn<Encargo, String> cNombrePE;
	@FXML
	private TableColumn<Encargo, String> cNombrePR;
	@FXML
	private TableColumn<Encargo, String> cEstado;
	
	
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnModificar;
	@FXML
	private Button btnEliminar;
	@FXML
	private Button btnActualizar;
	
	@FXML
	private TextField txtBuscar;
	
	@FXML
	private TextField txtId;
	@FXML
	private DatePicker dpFechaRegistrado;
	@FXML
	private DatePicker dpFechaEnvio;
	@FXML
	private DatePicker dpFechaRecepcion;
	@FXML
	private TextField txtIdPersona;
	@FXML
	private TextField txtDescripcion;
	@FXML
	private TextField txtValorTotal;
	@FXML
	private TextField txtPlacaVehiculo;
	@FXML
	private TextField txtNombrePT;
	@FXML
	private TextField txtNombrePEnvia;
	@FXML
	private TextField txtNombrePRecibe;
	@FXML
	private TextField txtEstado;
	
	@FXML
	private Button btnVerArbol;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		
		btnNuevo.setOnAction(e -> {
			actionNuevo();
		});
		
		btnModificar.setOnAction(e -> {
			actionModificar();
		});
		
		btnEliminar.setOnAction(e -> {
			actionEliminar();
		});
		
		btnActualizar.setOnAction(e -> {
			actionActualizar();
		});
		
		
		
		//configurar columnas
		
		encargoBsn = new EncargoBsnImpl();
		
		cId.setCellValueFactory(cellData -> cellData.getValue().getId().asObject());
		cFRegistrado.setCellValueFactory(cellData -> cellData.getValue().getFecha_registrado());
		cFEnvio.setCellValueFactory(cellData -> cellData.getValue().getFecha_envio());
		cFRecepcion.setCellValueFactory(cellData -> cellData.getValue().getFecha_recepcion());
		cIdPersona.setCellValueFactory(cellData -> cellData.getValue().getId_persona().asObject());
		cDescripcion.setCellValueFactory(cellData -> cellData.getValue().getDescripcion());
		cValorT.setCellValueFactory(cellData -> cellData.getValue().getValor_total().asObject());
		cPlacaV.setCellValueFactory(cellData -> cellData.getValue().getPlaca_vehiculo_transportador());
		cNombreT.setCellValueFactory(cellData -> cellData.getValue().getNombre_transportador());
		cNombrePE.setCellValueFactory(cellData -> cellData.getValue().getNombre_persona_envia());
		cNombrePR.setCellValueFactory(cellData -> cellData.getValue().getNombre_persona_recibe());
		cEstado.setCellValueFactory(cellData -> cellData.getValue().getEstado());
		
		actionActualizar();
		
		btnVerArbol.setOnAction(e -> {
			JOptionPane.showMessageDialog(null, "Recorrido INORDEN del arbol, por claves: "+encargoBsn.getInorden());
		});
	}

	private void actionActualizar() {
		// TODO Auto-generated method stub
		tEncargos.setItems(encargoBsn.getEncargos());
	}

	public void limpiarCampos(){
		txtId.setText("");
		dpFechaRegistrado.setValue(null);
		dpFechaEnvio.setValue(null);
		dpFechaRecepcion.setValue(null);
		txtIdPersona.setText("");
		txtDescripcion.setText("");
		txtValorTotal.setText("");
		txtPlacaVehiculo.setText("");
		txtNombrePT.setText("");
		txtNombrePEnvia.setText("");
		txtNombrePRecibe.setText("");
		txtEstado.setText("");
	}
	
	public void habilitarCampos() {
		txtId.setEditable(true);
		dpFechaRegistrado.setEditable(true);
		dpFechaEnvio.setEditable(true);
		dpFechaRecepcion.setEditable(true);
		txtIdPersona.setEditable(true);
		txtDescripcion.setEditable(true);
		txtValorTotal.setEditable(true);
		txtPlacaVehiculo.setEditable(true);
		txtNombrePT.setEditable(true);
		txtNombrePEnvia.setEditable(true);
		txtNombrePRecibe.setEditable(true);
		txtEstado.setEditable(true);
	}
	
	public void deshabilitarCampos() {
		txtId.setEditable(false);
		dpFechaRegistrado.setEditable(false);
		dpFechaEnvio.setEditable(false);
		dpFechaRecepcion.setEditable(false);
		txtIdPersona.setEditable(false);
		txtDescripcion.setEditable(false);
		txtValorTotal.setEditable(false);
		txtPlacaVehiculo.setEditable(false);
		txtNombrePT.setEditable(false);
		txtNombrePEnvia.setEditable(false);
		txtNombrePRecibe.setEditable(false);
		txtEstado.setEditable(false);
	}

	private void actionEliminar() {
		// TODO Auto-generated method stub
		try {
			encargoBsn.eliminarEnacargo(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		limpiarCampos();
	}

	private void actionModificar() {
		// TODO Auto-generated method stub
		Encargo encargo = new Encargo();
		
		encargo.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		encargo.setFecha_registrado(new SimpleObjectProperty<LocalDate>(dpFechaRegistrado.getValue()));
		encargo.setFecha_envio(new SimpleObjectProperty<LocalDate>(dpFechaEnvio.getValue()));
		encargo.setFecha_recepcion(new SimpleObjectProperty<LocalDate>(dpFechaRecepcion.getValue()));
		encargo.setId_persona(new SimpleIntegerProperty(Integer.valueOf(txtIdPersona.getText())));
		encargo.setDescripcion(new SimpleStringProperty(txtDescripcion.getText()));
		encargo.setValor_total(new SimpleIntegerProperty(Integer.valueOf(txtValorTotal.getText())));
		encargo.setPlaca_vehiculo_transportador(new SimpleStringProperty(txtPlacaVehiculo.getText()));
		encargo.setNombre_transportador(new SimpleStringProperty(txtNombrePT.getText()));
		encargo.setNombre_persona_envia(new SimpleStringProperty(txtNombrePEnvia.getText()));
		encargo.setNombre_persona_recibe(new SimpleStringProperty(txtNombrePRecibe.getText()));
		encargo.setEstado(new SimpleStringProperty(txtEstado.getText()));
		
		try {
			encargoBsn.modificarEncargo(encargo);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		
		limpiarCampos();
	}


	private void actionNuevo() {
		// TODO Auto-generated method stub
		Encargo encargo = new Encargo();
		
		encargo.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		encargo.setFecha_registrado(new SimpleObjectProperty<LocalDate>(dpFechaRegistrado.getValue()));
		encargo.setFecha_envio(new SimpleObjectProperty<LocalDate>(dpFechaEnvio.getValue()));
		encargo.setFecha_recepcion(new SimpleObjectProperty<LocalDate>(dpFechaRecepcion.getValue()));
		encargo.setId_persona(new SimpleIntegerProperty(Integer.valueOf(txtIdPersona.getText())));
		encargo.setDescripcion(new SimpleStringProperty(txtDescripcion.getText()));
		encargo.setValor_total(new SimpleIntegerProperty(Integer.valueOf(txtValorTotal.getText())));
		encargo.setPlaca_vehiculo_transportador(new SimpleStringProperty(txtPlacaVehiculo.getText()));
		encargo.setNombre_transportador(new SimpleStringProperty(txtNombrePT.getText()));
		encargo.setNombre_persona_envia(new SimpleStringProperty(txtNombrePEnvia.getText()));
		encargo.setNombre_persona_recibe(new SimpleStringProperty(txtNombrePRecibe.getText()));
		encargo.setEstado(new SimpleStringProperty(txtEstado.getText()));
		
		try {
			encargoBsn.crearEncargo(encargo);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave duplicada. No se guardo el registro.");
		}
		
		limpiarCampos();
	}
}
