package udea.edu.co.app.controlador;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;


import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.Persona;
import udea.edu.co.app.negocio.PersonaBsn;
import udea.edu.co.app.negocio.impl.PersonaBsnImpl;

public class PersonaControlador implements Initializable{
	
	@FXML
	private TableView<Persona> tPersona;
	@FXML
	private TableColumn<Persona, Integer> cId;
	@FXML
	private TableColumn<Persona, String> cTipo;
	@FXML
	private TableColumn<Persona, String> cNombres;
	@FXML
	private TableColumn<Persona, String> cApellidos;
	@FXML
	private TableColumn<Persona, String> cEmail;
	@FXML
	private TableColumn<Persona, String> cTelefono;
	@FXML
	private TableColumn<Persona, String> cCompania;
	@FXML
	private TableColumn<Persona, String> cDireccion;
	
	@FXML
	private Button btnVerArbol;
	
	private PersonaBsn personaBsn;
	
	//botones
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnModificar;
	@FXML
	private Button btnEliminar;
	@FXML
	private Button btnActualizar;
	
	//campos de texto
	@FXML
	private TextField txtBuscar;
	@FXML
	private TextField txtId;
	@FXML
	private TextField txtTipo;
	@FXML
	private TextField txtNombres;
	@FXML
	private TextField txtApellidos;
	@FXML
	private TextField txtEmail;
	@FXML
	private TextField txtTelefono;
	@FXML
	private TextField txtCompania;
	@FXML
	private TextField txtDireccion;
	
	
	
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		personaBsn = new PersonaBsnImpl();
		
		//al accionar los botones
		btnNuevo.setOnAction(e -> {
			actionNuevo();
		});
		
		
		btnModificar.setOnAction(e -> {
			actionModificar();
		});
		
		btnEliminar.setOnAction(e -> {
			actionEliminar();
		});
		
		btnActualizar.setOnAction(e -> {
			actionActualizar();
		});
		
		actionActualizar();
		
		//Configuracion de las columnas
		cId.setCellValueFactory(cellData -> cellData.getValue().getId().asObject());
		cTipo.setCellValueFactory(cellData -> cellData.getValue().getTipo());
		cNombres.setCellValueFactory(cellData -> cellData.getValue().getNombres());
		cApellidos.setCellValueFactory(cellData -> cellData.getValue().getApellidos());
		cEmail.setCellValueFactory(cellData -> cellData.getValue().getEmail());
		cTelefono.setCellValueFactory(cellData -> cellData.getValue().getTelefono());
		cCompania.setCellValueFactory(cellData -> cellData.getValue().getCompania_representada());
		cDireccion.setCellValueFactory(cellData -> cellData.getValue().getDireccion());
		
		btnVerArbol.setOnAction(e -> {
			JOptionPane.showMessageDialog(null, "Recorrido INORDEN del arbol, por claves: "+personaBsn.getInorden());
		});
	}
	
	private void actionActualizar() {
		// TODO Auto-generated method stub
		tPersona.setItems(personaBsn.getPersonas());
	}

	public void limpiarCampos(){
		txtId.setText("");
		txtTipo.setText("");
		txtNombres.setText("");
		txtApellidos.setText("");
		txtEmail.setText("");
		txtTelefono.setText("");
		txtCompania.setText("");
		txtDireccion.setText("");
	}
	
	public void habilitarCampos() {
		txtId.setEditable(true);
		txtTipo.setEditable(true);
		txtNombres.setEditable(true);
		txtApellidos.setEditable(true);
		txtEmail.setEditable(true);
		txtTelefono.setEditable(true);
		txtCompania.setEditable(true);
		txtDireccion.setEditable(true);
	}
	
	public void deshabilitarCampos() {
		txtId.setEditable(false);
		txtTipo.setEditable(false);
		txtNombres.setEditable(false);
		txtApellidos.setEditable(false);
		txtEmail.setEditable(false);
		txtTelefono.setEditable(false);
		txtCompania.setEditable(false);
		txtDireccion.setEditable(false);
	}

	private void actionEliminar() {
		// TODO Auto-generated method stub
		try {
			personaBsn.eliminarPersona(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		} catch (NumberFormatException | LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		limpiarCampos();
	}

	private void actionModificar() {
		// TODO Auto-generated method stub
		Persona persona = new Persona();
		
		
		persona.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		persona.setNombres(new SimpleStringProperty(txtNombres.getText()));
		persona.setApellidos(new SimpleStringProperty(txtApellidos.getText()));
		persona.setCompania_representada(new SimpleStringProperty(txtCompania.getText()));
		persona.setEmail(new SimpleStringProperty(txtEmail.getText()));
		persona.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
		persona.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
		persona.setTipo(new SimpleStringProperty(txtTipo.getText()));
		try {
			personaBsn.modificarPersona(persona);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		
		limpiarCampos();
		
		
	}


	private void actionNuevo() {
		// TODO Auto-generated method stub
		Persona persona = new Persona();
		
		
		persona.setId(new SimpleIntegerProperty(Integer.parseInt(txtId.getText())));
		persona.setNombres(new SimpleStringProperty(txtNombres.getText()));
		persona.setApellidos(new SimpleStringProperty(txtApellidos.getText()));
		persona.setCompania_representada(new SimpleStringProperty(txtCompania.getText()));
		persona.setEmail(new SimpleStringProperty(txtEmail.getText()));
		persona.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
		persona.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
		persona.setTipo(new SimpleStringProperty(txtTipo.getText()));
		try {
			personaBsn.crearPersona(persona);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave duplicada. No se guardo el registro.");
		}
		
		limpiarCampos();
	}

}
