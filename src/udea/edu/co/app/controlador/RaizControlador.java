package udea.edu.co.app.controlador;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import udea.edu.co.app.MainApp;

public class RaizControlador implements Initializable{
	
	@FXML
	private Button bPrincipal;
	@FXML
	private Button bPersona;
	@FXML
	private Button bLicor;
	@FXML
	private Button bInventario;
	@FXML
	private Button bEncargo;
	@FXML
	private Button bDetalleEncargo;
	
	private MainApp main;
	
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		//al presionar botones
		bPrincipal.setOnAction(e ->{
			accionBtnPrincipal();
		});
		
		bPersona.setOnAction(e -> {
			accionBtnPersona();
		});
		
		bLicor.setOnAction(e -> {
			accionBtnLicor();
		});
		
		bInventario.setOnAction(e -> {
			accionBtnInventario();
		});
		
		bEncargo.setOnAction(e -> {
			accionBtnEncargo();
		});
		
		bDetalleEncargo.setOnAction(e -> {
			accionBtnDetalleEncargo();
		});
		
	}

	private void accionBtnDetalleEncargo() {
		// TODO Auto-generated method stub
		main.cargarDetalleEncargo();
	}

	private void accionBtnEncargo() {
		// TODO Auto-generated method stub
		main.cargarVistaEncargo();
	}

	private void accionBtnInventario() {
		// TODO Auto-generated method stub
		main.cargarVistaInventario();
	}

	private void accionBtnLicor() {
		// TODO Auto-generated method stub
		main.cargarVistaLicor();
	}

	private void accionBtnPersona() {
		// TODO Auto-generated method stub
		main.cargarVistaPersona();
	}

	private void accionBtnPrincipal() {
		// TODO Auto-generated method stub
		main.cargarVistaPrincipal();
	}
	
	public void setMain(MainApp mainA) {
		main = mainA;
	}

}
