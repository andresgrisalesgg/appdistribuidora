package udea.edu.co.app.controlador;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import udea.edu.co.app.dao.exception.LlaveDuplicadaException;
import udea.edu.co.app.dao.exception.LlaveNoExiste;
import udea.edu.co.app.modelo.DetalleEncargo;
import udea.edu.co.app.negocio.DetalleEncargoBsn;
import udea.edu.co.app.negocio.impl.DetalleEncargoBsnImpl;

public class DetalleEncargoControlador implements Initializable{
	
	DetalleEncargoBsn detalleEncargoBsn;
	
	@FXML
	private TableView<DetalleEncargo> tDetallesE;
	@FXML
	private TableColumn<DetalleEncargo, Integer> cLote;
	@FXML
	private TableColumn<DetalleEncargo, Integer> cIdEncargo;
	@FXML
	private TableColumn<DetalleEncargo, Integer> cIdLicor;
	@FXML
	private TableColumn<DetalleEncargo, Integer> cNumeroUnidades;
	@FXML
	private TableColumn<DetalleEncargo, LocalDate> cFechaE;
	@FXML
	private TableColumn<DetalleEncargo, Integer> cValorT;
	
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnModificar;
	@FXML
	private Button btnEliminar;
	@FXML
	private Button btnActualizar;
	
	@FXML
	private TextField txtBuscar;
	
	@FXML
	private TextField txtLote;
	@FXML
	private TextField txtIdEncargo;
	@FXML
	private TextField txtIdLicor;
	@FXML
	private TextField txtNumeroUnidades;
	@FXML
	private TextField txtValorTotal;
	@FXML
	private DatePicker dpFechaElaboracion;
	
	@FXML
	private Button btnVerArbol;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		detalleEncargoBsn = new DetalleEncargoBsnImpl();
		
		btnNuevo.setOnAction(e -> {
			actionNuevo();
		});
		
		btnModificar.setOnAction(e -> {
			actionModificar();
		});
		
		btnEliminar.setOnAction(e -> {
			actionEliminar();
		});
		
		btnActualizar.setOnAction(e -> {
			actionActualizar();
		});
		
		actionActualizar();
		
		cLote.setCellValueFactory(cellData -> cellData.getValue().getLote().asObject());
		cIdEncargo.setCellValueFactory(cellData -> cellData.getValue().getId_encargo().asObject());
		cIdLicor.setCellValueFactory(cellData -> cellData.getValue().getId_licor().asObject());
		cNumeroUnidades.setCellValueFactory(cellData -> cellData.getValue().getNumero_unidades().asObject());
		cFechaE.setCellValueFactory(cellData -> cellData.getValue().getFecha_elaboracion());
		cValorT.setCellValueFactory(cellData -> cellData.getValue().getValor_total().asObject());
		
		btnVerArbol.setOnAction(e -> {
			JOptionPane.showMessageDialog(null, "Recorrido INORDEN del arbol, por claves: "+detalleEncargoBsn.getInorden());
		});
	}
	private void actionActualizar() {
		// TODO Auto-generated method stub
		tDetallesE.setItems(detalleEncargoBsn.getDetalleEncargos());
	}
	public void limpiarCampos(){
		txtLote.setText("");
		txtIdEncargo.setText("");
		txtIdLicor.setText("");
		txtNumeroUnidades.setText("");
		txtValorTotal.setText("");
		dpFechaElaboracion.setValue(null);
	}
	
	public void habilitarCampos() {
		txtLote.setEditable(true);
		txtIdEncargo.setEditable(true);
		txtIdLicor.setEditable(true);
		txtNumeroUnidades.setEditable(true);
		txtValorTotal.setEditable(true);
		dpFechaElaboracion.setEditable(true);
	}
	
	public void deshabilitarCampos() {
		txtLote.setEditable(false);
		txtIdEncargo.setEditable(false);
		txtIdLicor.setEditable(false);
		txtNumeroUnidades.setEditable(false);
		txtValorTotal.setEditable(false);
		dpFechaElaboracion.setEditable(false);
	}

	private void actionEliminar() {
		// TODO Auto-generated method stub
		try {
			detalleEncargoBsn.eliminarDetalleEncargo(new SimpleIntegerProperty(Integer.parseInt(txtLote.getText())));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		limpiarCampos();
	}

	private void actionModificar() {
		// TODO Auto-generated method stub
		DetalleEncargo detalleEncargo = new DetalleEncargo();
		
		detalleEncargo.setLote(new SimpleIntegerProperty(Integer.parseInt(txtLote.getText())));
		detalleEncargo.setId_encargo(new SimpleIntegerProperty(Integer.parseInt(txtIdEncargo.getText())));
		detalleEncargo.setId_licor(new SimpleIntegerProperty(Integer.parseInt(txtIdLicor.getText())));
		detalleEncargo.setNumero_unidades(new SimpleIntegerProperty(Integer.parseInt(txtNumeroUnidades.getText())));
		detalleEncargo.setValor_total(new SimpleIntegerProperty(Integer.parseInt(txtValorTotal.getText())));
		detalleEncargo.setFecha_elaboracion(new SimpleObjectProperty<LocalDate>(dpFechaElaboracion.getValue()));
		
		try {
			detalleEncargoBsn.modificarDetalleEncargo(detalleEncargo);
		} catch (LlaveNoExiste e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave no existe");
		}
		
		limpiarCampos();
	}

	private void actionNuevo() {
		// TODO Auto-generated method stub
		DetalleEncargo detalleEncargo = new DetalleEncargo();
		
		detalleEncargo.setLote(new SimpleIntegerProperty(Integer.parseInt(txtLote.getText())));
		detalleEncargo.setId_encargo(new SimpleIntegerProperty(Integer.parseInt(txtIdEncargo.getText())));
		detalleEncargo.setId_licor(new SimpleIntegerProperty(Integer.parseInt(txtIdLicor.getText())));
		detalleEncargo.setNumero_unidades(new SimpleIntegerProperty(Integer.parseInt(txtNumeroUnidades.getText())));
		detalleEncargo.setValor_total(new SimpleIntegerProperty(Integer.parseInt(txtValorTotal.getText())));
		detalleEncargo.setFecha_elaboracion(new SimpleObjectProperty<LocalDate>(dpFechaElaboracion.getValue()));
		
		try {
			detalleEncargoBsn.crearDetalleEncargo(detalleEncargo);
		} catch (LlaveDuplicadaException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Llave duplicada. No se guardo el registro.");
		}
		
		limpiarCampos();
	}
	
}
